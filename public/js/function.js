// view all student query search
$("#kt_datatable_search_query").keyup(function(event){
	event.preventDefault();
	var searchval = $('#kt_datatable_search_query').val();
	var count = searchval.length;
	if(count >= 0){
		customSearch(searchval);
	}
});

/* SEARCHING student details*/
function customSearch(searchval){
	var formData = {searchval:searchval};
	$.ajax({
		type: "GET",
		url: 'https://vkapsprojects.com/nitesh/student-management/search-student-record',
		data: formData,
		dataType: 'json',
		success: function (data) {
			if(data.status == true){
				$('#searchDataTable').html('');
				var resData = data.success;
				var html = '';
				$.each( resData, function( key, value ){
					key=key+1;
					$('#searchDataTable').append('<tr class="text-center"><td>'+key+'</td><td>'+value.name+'</td><td>'+value.email+'</td><td>'+value.age+'</td><td>'+value.gender+'</td><td><a href="edit/'+value.id+'" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle mx-3"><i class="fa fa-pencil-alt"></i></a><a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Delete" class="btn mx-3 btn-danger btn-circle" onclick="confirmDeleteData('+value.id+')"><i class="fa fa-times"></i></a><a href="show-student/'+value.id+'" data-toggle="tooltip" data-original-title="View All Detail" class=" mx-3 btn btn-primary btn-circle"><i class="fa fa-eye"></i> </a></td></tr>');
				});
			}
		},
		error: function (data) {
			console.log(data);	
		}
	});
}

/* DELETE */
function confirmDeleteData(id, name) {
    Swal.fire({
        title: 'Are you sure want to delete '+name+'?',
        text: "You won't be able to revert this!", 
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            $.ajax('https://vkapsprojects.com/nitesh/student-management/delete-student/'+id, {
                method: 'GET',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            }).done(function (r) {
                if(r == 'success'){
                    // $("div.delete_message").html('<div class="alert alert-success">Student record deleted successfully.</div>');
					Swal.fire(
					  'Student record deleted successfully!',
					  '',
					  'success'
					)
                    window.setTimeout(function(){location.reload()},2000);
                }
                else{                
                    swal.fire({
                        text: 'Customer does not exist..',
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                    }).then(function () {
                        KTUtil.scrollTop();
                    });            
                }    
            }).fail(function (jqXHR, textStatus, errorThrown) {             
                swal.fire({
                    text: errors,
                    icon: "error",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn font-weight-bold btn-light-primary"
                    }
                }).then(function () {
                    KTUtil.scrollTop();
                });    
            });
        }
    });
}




$(document).ready(function(){


	// add more semester of students
	var semesterRow=1;
	$('#addSemester').click(function(index,item){
		
		$('#addMoreSemester').append('<div class="row" id="semester"><div class="col-md-4" ><div class="form-group"><label>Semester</label><input type="text" name="studentSemesterName[]" id="studentSemesterName" class="form-control"></div></div><div class="col-md-2"><div class="form-group"><label>Grade</label><input type="text" name="studentSemesterGrade[]" id="studentSemesterGrade'+semesterRow+'" class="form-control"></div></div><div class="col-md-4"><div class="form-group"><label>Upload File</label><input type="file" name="studentSemesterFile[]" id="studentSemesterFile" class="semesterFileCheck form-control-file" value="{{old("studentSemesterFile")}}"></div></div><div class="col-md-1"><button  class="btn btn-danger" id="deleteSemester" type="button">X</button></div></div>');
		semesterRow++;
		// $('#addSemester').hide();	
	});
	$('#addMoreSemester').on('click','#deleteSemester',function(e){
		// $('#achievement3').remove();
		if(confirm("do you want to delete semester field?"))
		{
			$(this).parents('#semester').remove();
		}
		if($('#addMoreSemester').val()=="")
		{
			$('#addSemester').show();
		}
		else{
			$('#addSemester').hide();
		}
	});
	$('#addMoreSemester').on('mouseleave','#studentSemesterFile',function(){
		if($(this).val()!="")
		{				
			$('#addSemester').show();
			$('#semester_error').hide();

		}
		// else{
		// 	$('#addSemester').hide();
		// }
	});



	// add other information of student
	var otherInformation=1;
	$('#addOtherInformation').click(function(){
		$('#addMoreOtherInformation').append('	<div class="row " id="information">	<div class="col-md-6"><div class="form-group"><label>Other Information</label><input type="text" name="otherInformationName[]" id="otherInformationName'+otherInformation+'" class="form-control"></div></div><div class="col-md-4"><div class="form-group"><label>Upload File</label><input type="file" name="otherInformatonFile[]" id="otherInformatonFile" class="form-control-file"></div></div><div class="col-md-1"><button  class="btn btn-danger" id="deleteOtherInformation">X</button></div></div>');
		otherInformation++;
		// $('#addOtherInformation').hide();
	});
	$('#addMoreOtherInformation').on('click','#deleteOtherInformation',function(e){
		if(confirm("Do you really want to delete other information field?"))
		{
			$(this).parents('#information').remove();
		}
		if($("#addMoreOtherInformation").val()=="")
		{
			$("#addOtherInformation").show();
		}
		else{
			$("#addOtherInformation").hide();
		}
	});
	$('#addMoreOtherInformation').on('mouseleave','#otherInformatonFile',function(){
		if($(this).val()!="")
		{
			$('#addOtherInformation').show();
		}
		// else{
		// 	$("#addOtherInformation").hide();
		// }
	});
	// add more school achievement of students
	var schoolAchievement=1;
	$('#addSchoolAchievement').click(function(){
		
		$('#addMoreSchoolAchievement').append('<div class="row " id="achievement"><div class="col-md-6"><div class="form-group"><label>School Achievement</label><input type="text"name="schoolAchievementName[]" id="schoolAchievementName'+schoolAchievement+'" class="form-control"></div></div><div class="col-md-4"><div class="form-group"><label>Upload File</label><input type="file" name="schoolAchievementFile[]" id="schoolAchievementFile" class="form-control-file"></div></div><div class="col-md-1"><button  class="btn btn-danger" id="deleteMoreAchievement">X</button></div></div>');
		schoolAchievement++;
		// $('#addSchoolAchievement').hide();
	});

	$('#addMoreSchoolAchievement').on('click','#deleteMoreAchievement',function(e){
		// $('#achievement3').remove();
		if(confirm("Do you really want to delete school achievement field?"))
		{
			$(this).parents('#achievement').remove();
		}
		if($('#addMoreSchoolAchievement').val()=="")
		{
			$('#addSchoolAchievement').show();
		}
		else{
			$('#addSchoolAchievement').hide();
		}
	});

	$('#addMoreSchoolAchievement').on('mouseleave','#schoolAchievementFile',function(){
		if($(this).val()!="")
		{
			$('#addSchoolAchievement').show();
		}
		// else{
		// 	$('#addSchoolAchievement').hide();
		// }
	});
	
	$('.monthlySetCourse').hide();	
	$("body").on("change", "input[type=radio].sh", function() {
		var typeOfClasses = $('.typeOfClasses').val();
		if ($(this).val() == "setsch") {
			if(typeOfClasses == 'monthly'){
				// console.log(typeOfClasses);
				$(".monthlySetCourse").show();
				$(".weeklySetCourse").hide();
			}else{
				// console.log(typeOfClasses);
				$(".weeklySetCourse").show();
				$(".monthlySetCourse").hide();
			}
		}
    });

 $('#typeOfClasses').change(function(){
		$data=$('#typeOfClasses').val();
		if($data=='monthly')
		{
			$('#monthlySetCourse').show();
			$('#weeklySetCourse').hide()
		}
		else
		{
			$('#monthlySetCourse').hide();
			$('#weeklySetCourse').show()
		}
	});
    $("input[type=radio].free-sechdule").change(function() {
        if ($(this).val() == "freesch") {
            $(".otherAnswer").show();
            $(".weeklySetCourse").hide();
            $(".monthlySetCourse").hide();
        } else {
            $(".otherAnswer").hide();

        }
    });

    $(".typeOfClasses").change(function(){
        var optionValue = $(this).val();
        var freesch = $('.freesch').val();
        var setsch = $('.setsch').val();
        // console.log(optionValue);
        if ($('.setsch:checked').length > 0) {
        	// console.log(setsch);
        	// console.log(freesch);
        	if((setsch == 'setsch')){
		        if(optionValue == 'monthly'){
					// console.log(typeOfClasses);
					$(".monthlySetCourse").show();
					$(".weeklySetCourse").hide();
				}else{
					// console.log(typeOfClasses);
					$(".weeklySetCourse").show();
					$(".monthlySetCourse").hide();
				}
			}else if(freesch != 'freesch') {
				$(".monthlySetCourse").hide();
				$(".weeklySetCourse").hide();
			}
		}
    });

});
// search option in all select box
$(document).ready(function () {
//change selectboxes to selectize mode to be searchable
   $("#searchSelect").select2();
});

/* course Information */

// onKeySum for calculate hours and days . 
function onkeyupsum() { // calculate sum and show in textbox
	var sum = document.querySelector('#numOfClasses').value;
	var number = document.querySelector('#numOfHours').value;
	val=sum*number;
	document.submitform.tHours.value = val;
}


//  fade out on all alert box

$('.alert-dismissible').fadeOut(5000);



// set schedule function 
/*===========================Weekly Schedule======================================================*/

$('#addWeeklySchedule').click(function(){
	$('#addWeeeklyMoreSchedule').append('<div class="row" id="weeklySchedule"><div class="col-md-8 mt-4"><label><b>Weekly Day Classes</b></label><select class="form-control" name="weeklyDayClasses"><option value="">Select</option><option value="monday">Monday</option><option value="tuesday">Tuesday</option><option value="wedesday">Wedesday</option><option value="thursday">Thursday</option><option value="friday">Friday</option><option value="saturday">Saturday</option><option value="sunday">Sunday</option></select></div><div class="col-md-4"><button type="button" class="btn btn-outline-danger float-right mt-4" id="removeWeeklySchedule"><i class="fas fa-trash"></i></button></div><div class="col-md-6 mt-4"><label>Set Schedule Date</label><input type="date" name="scheduleCalender" class="form-control" "></div><div class="col-md-3 mt-4"><label>Set Schedule From</label><select class="form-control" name="weeklyDayTime"><option value="">Select</option><option value="10am">10:00 AM</option><option value="11am" >11:00 AM</option><option value="12pm" >12:00 PM</option><option value="1pm">1:00 PM</option><option value="2pm" >2:00 PM</option><option value="3pm" >3:00 PM</option><option value="4pm" >4:00 PM</option><option value="5pm" >5:00 AM</option><option value="6pm" >6:00 AM</option></select></div><div class="col-md-3 mt-4"><label>Set Schedule To</label><select class="form-control" name="weeklyDaysTime"><option value="">Select</option><option value="11am" >11:00 AM</option><option value="12pm">12:00 PM</option><option value="1pm">1:00 PM</option><option value="2pm">2:00 PM</option><option value="3pm">3:00 PM</option><option value="4pm">4:00 PM</option><option value="5pm">5:00 AM</option><option value="6pm">6:00 AM</option><option value="7pm">7:00 PM</option></select></div></div>');
});

$('#addWeeeklyMoreSchedule').on('click','#removeWeeklySchedule',function(e){
	$(this).parents('#weeklySchedule').remove()
});	
/*===========================End Weekly Schedule======================================================*/

/*===========================Monthly Schedule======================================================*/

$('#addMonthlySchedule').click(function(){
	$('#addMonthlyMoreSchedule').append('<div class="row" id="monthlySchedule"><div class="col-md-8 mt-4"><label><b>Monthly Day Classes</b></label><select class="form-control" name="monthlyDayClasses"><option value="">Select</option><option value="monday">Monday</option><option value="tuesday">Tuesday</option><option value="wedesday">Wedesday</option><option value="thursday">Thursday</option><option value="friday">Friday</option><option value="saturday">Saturday</option><option value="sunday">Sunday</option></select></div><div class="col-md-4"><button type="button" class="btn btn-outline-danger float-right mt-4" id="removeMonthlySchedule"><i class="fas fa-trash"></i></button></div><div class="col-md-6 mt-4"><label>Set Schedule Date</label><input type="date" name="scheduleCalender" class="form-control" "></div><div class="col-md-3 mt-4"><label>Set Schedule From</label><select class="form-control" name="monthlyDayTime"><option value="">Select</option><option value="10am">10:00 AM</option><option value="11am" >11:00 AM</option><option value="12pm" >12:00 PM</option><option value="1pm">1:00 PM</option><option value="2pm" >2:00 PM</option><option value="3pm" >3:00 PM</option><option value="4pm" >4:00 PM</option><option value="5pm" >5:00 AM</option><option value="6pm" >6:00 AM</option></select></div><div class="col-md-3 mt-4"><label>Set Schedule To</label><select class="form-control" name="monthlyDaysTime"><option value="">Select</option><option value="11am" >11:00 AM</option><option value="12pm">12:00 PM</option><option value="1pm">1:00 PM</option><option value="2pm">2:00 PM</option><option value="3pm">3:00 PM</option><option value="4pm">4:00 PM</option><option value="5pm">5:00 AM</option><option value="6pm">6:00 AM</option><option value="7pm">7:00 PM</option></select></div></div>');
});

$('#addMonthlyMoreSchedule').on('click','#removeMonthlySchedule',function(e){
	$(this).parents('#monthlySchedule').remove()
});	

/*=========================== End Monthly Schedule ================================================*/



/*============================ add course Schedule ================================================*/
var sectionClone = $(".section").clone();
$("#addMoreCourseSchedule").on("click", function(){
	var totalSection = $("div.section").length;
	var newClone = sectionClone.clone().find("input[type=radio]").each(function(index){
		var name = $(this).attr("name");
		name = name+"-"+totalSection;
		$(this).attr("name", name);
	}).end();
	$(".section").after(newClone.html());
})

/*============================ End add course Schedule ================================================*/

// ========== display schedule data which selected by students

$('#weeklyDayClasses').change(function(){
	var classes=$('#weeklyDayClasses').val();
	console.log(classes);
	$('#showScheduleData').append('<tr><td>'+classes+'</td><td>dsfsdf</td><td>dsfsdfs</td><td>sdfdsf</td></tr>');
})

$('#scheduleCalender').change(function(){
	var date=$('#scheduleCalender').val();
	$('#scheduleCalender').append(date);
})


/*free sehdule coding*/
$(function () {
    $("#courseStartDate").datepicker({
        numberOfMonths: 2,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#txtTo").datepicker("option", "minDate", dt);
        }
    });
    $("#courseEndDate").datepicker({
        numberOfMonths: 2,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#txtFrom").datepicker("option", "maxDate", dt);
        }
    });
});