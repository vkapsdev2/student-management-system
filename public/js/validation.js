$(document).ready(function(){

  $('#save_student_details').validate({
    errorClass: "error fail-alert",
    validClass: "valid success-alert",
  rules :{
    name : {
      required : true,
    },
    age:{
      required:true,
      number    : true,
      maxlength:2
    },
    gender:{
      required:true,
    },
    dob:{
      required:true,
    },
    faxNumber:{
      required:true,
      number    : true,
      minlength:10,
      maxlength:12
    },
    homeNumber:{
      required:true,
      number    : true,
      minlength:10,
      maxlength:12
    },
    mobileNumber:{
      required:true,
      number    : true,
      minlength:10,
      maxlength:12
    },
    email:{
      required:true,
      email:true
    },
    address:{
      required:true,
      minlength:10
    },
    emergencyName:{
      required:true,
    },
    emergencyEmail:{
      required:true
    },
    emergencyGender:{
      required:true
    },
    emergencyRelationshipStudent:{
      required:true
    },
    emergencyCellNumber:{
      required:true,
      number    : true,
      minlength:10,
      maxlength:12
    },
    emergencyHomeNumber:{
      required:true,
      number    : true,
      minlength:10,
      maxlength:12
    },
    emergencyFaxNumber:{
      required:true,
      number    : true,
      minlength:10,
      maxlength:12
    },
    emergencyAddress:{
      required:true,
      minlength:10
    },
    
  },
  messages : {
    name : {
      required : "Please enter name.",
    },
    age:{
      required:'Please enter age',
    },
    gender:{
      required:'Please select gender',
    },
    faxNumber:{
      required:'Please enter fax number',
    },
    homeNumber:{
      required:'Please enter home number',
    },
    mobileNumber:{
      required:'Please enter mobile number',
    },
    email:{
      required:'Please enter email',
    },
    address:{
      required:'Please enter address',
    },
    emergencyName:{
      required:'Please enter name',
    },
    emergencyEmail:{
      required:"Please enter email"
    },
    emergencyGender:{
      required:'Please select gender',
    },
    emergencyAddress:{
      required:'Please enter address',
    },
    emergencyFaxNumber:{
      required:'Please enter fax number',
    },
    emergencyHomeNumber:{
      required:'Please enter home number',
    },
    emergencyCellNumber:{
      required:'Please enter mobile number',
    },
    emergencyRelationshipStudent:{
      required:"Please select relationship"
    }
  },
  submitHandler: function(form) {
        form.submit();
    }
    
});
// acadmic validation start
$('#academic_information').validate({
  errorClass: "error fail-alert",
  validClass: "valid success-alert",
  rules:{
    studentId:{
      required:true
    },
    schoolName:{
      required:true,
    },
    studentStandard:{
      required:true,
    },
    studentClass:{
      required:true,
    },
    'studentSemesterName[]':{
      required:true,
    },
    'studentSemesterGrade[]':{
      required:true,
      number:true
    },
    'studentSemesterFile[]':{
      required:true,
      extension: "jpg,png,doc,pdf",
    },
    'otherInformatonFile[]':{
      required:true,
      extension: "jpg,png,doc,pdf",
    },
    'schoolAchievementFile[]':{
      required:true,
      extension: "jpg,png,doc,pdf",
    }
  },
  messages:{
    studentId:{
      required:'Please select student'
    },
    schoolName:{
      required:"Please enter school name"
    },
    studentStandard:{
      required:"Please enter student standard"
    },
    studentClass:{
      required:"Please enter class"
    },
    'studentSemesterName[]':{
      required:'Please enter semester name'
    },
    'studentSemesterGrade[]':{
      required:"Please enter grade",
      number:"Only accept number & decimal value"
    },
    'studentSemesterFile[]':{
      required:"please select image",
      extension:"Only jpg,png,pdf,doc file support"
    },
    'otherInformatonFile[]':{
      required:"please select image",
      extension:"Only jpg,png,pdf,doc file support"
    },
    'schoolAchievementFile[]':{
      required:"please select image",
      extension:"Only jpg,png,pdf,doc file support"
    }
  },
  submitHandler: function(form) {
    form.submit();  
    }
});

// $('#update-academic_information').validate({
//   errorClass: "error fail-alert",
//   validClass: "valid success-alert",
//   rules:{
//     'studentSemesterName[]':{
//       required:true,
//     },
//   },
//   submitHandler: function(form) {
//     form.submit();  
//     }
// });
// academic validation end


// another


});