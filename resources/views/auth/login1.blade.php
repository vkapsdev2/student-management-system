
@extends('layout.default')
@section('content')


<div class="container-fluid">
    <div class="col-md-10 mx-auto">
        <h2 class="text-center">Student Login Form</h2>
        <hr>
        @if(session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>
        @endif
        @if(session('error'))
        <div class="alert alert-danger">
          {{session('error')}}
        </div>
        @endif
        <form action="{{ route('login') }}" method="post">
            @csrf
            <div class="form-group">

                <div class="row">
                    <div class="col-md-6 mx-auto mt-2">
                    <label>E-mail</label>
                        <input type="text" name="email" value="{{ old('email') }}" class="form-control" placeholder="Enter Email" id="email">
                        @error('email')
                            <p id="nameerr">
                                <p class="alert alert-danger mt-1">
                                    {{$message}}
                                </p>
                            </p>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-6 mt-2 mx-auto">
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" id="password" class="form-control" value="{{old('password')}}">
                        @error('password')
                            <p class="alert alert-danger mt-1">{{$message}}</p>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="text-center mt-4">
                <input type="submit" name="" value="Login" class="btn btn-outline-success">
            </div>
        </form>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

@endsection