@extends('layouts.app')
@section('content')


<div class="container-fluid">
    <div class="img-icon col-md-12 text-center">
        <img src="{{ asset('images/site_1.png') }}" height="200" width="200">
    </div>

    <div class="col-md-10 mx-auto account-heading-txt">
            <h2 class="text-center">{{ __('Login') }}</h2>
            <p class="text-center">Enter your details to login to your account:</p>

            @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
            @endif
            @if(session('error'))
            <div class="alert alert-danger">
              {{session('error')}}
            </div>
            @endif
        <form class="custom-form" action="{{ route('login') }}" method="post">
            @csrf
            <div class="form-group ">
                <div class="row">

                    <div class="col-md-6 mx-auto mt-2">
                        <!-- <label>E-mail</label> -->
                        <input type="text" name="email" value="{{ old('email') }}" class="form-control custom-input" placeholder="Enter Email" id="email">
                        @error('email')
                        <span class="alert alert-danger mt-1">{{$message}}</span>
                        <!-- <p id="nameerr">
                            <p class="alert alert-danger mt-1">
                                {{$message}}
                            </p>
                        </p> -->
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-6 mt-2 mx-auto">
                    <div class="form-group">
                        <!-- <label>Password</label> -->
                        <input type="password" name="password" id="password" class="form-control custom-input" placeholder="Enter Password" value="{{old('password')}}">
                        @error('password')
                        <span class="alert alert-danger mt-1">{{$message}}</span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6 offset-md-3">
                    <div class="form-check">
                        <input class="custom-check form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
            </div>
            <div class="text-center mt-4">
                <input type="submit" name="" value="Login" class="custom-btn btn btn-outline-success">

            </div>
        </form>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

@endsection
