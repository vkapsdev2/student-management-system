{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}

@section('content')

    {{-- Dashboard 1 --}}
    <div class="row">
        @if (Auth::user()->hasRole('Admin'))

            <div class="col-lg-12 col-xxl-4">
                @include('pages.widgets._widget-1', ['class' => 'card-stretch gutter-b'])
            </div>
        @elseif (Auth::user()->hasRole('Staff'))
            <h1>Staff</h1>
        @elseif (Auth::user()->hasRole('Student'))
            <h1>Student</h1>
        @endif
    </div>





@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
@endsection
