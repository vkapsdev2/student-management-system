{{-- Advance Table Widget 2 --}}

<div class="card card-custom {{ @$class }}">
    {{-- Header --}}
    <div class="card-header border-0 pt-5">
        <h3 class="card-title align-items-start flex-column">
            <span class="card-label font-weight-bolder text-dark">Course Single Details</span>
        </h3>
    </div>

    {{-- Body --}}
    <div class="card-body pt-3 pb-0">
        {{-- Table --}}
        <div class="table-responsive">
            <div class="row">
                <div class="col-xl-12">
                    <h3 class=" text-dark font-weight-bold mb-10"></h3>
                    <div class="form-group ">
                        <label class="col-3 col-form-label">Student Name</label>
                               {{ $student_data ? '$student->name' : "-" }}
                    </div>
                    <div class="form-group ">
                        <label class="col-3 col-form-label">Course Name</label>
                        {{ $coursesData->courseName }}
                    </div>
                    <div class="form-group">
                        <label class="col-3 col-form-label">Staff Incharge</label>
                         {{$coursesData->staffIncharge}}
                    </div>
                    <div class="form-group">
                        <label class="col-3">Supporting Staff</label>
                        {{$coursesData->supportStaff}}
                    </div>
                    <div class="form-group">
                        <label class="col-3"> Type Of Classes</label>
                        {{$coursesData->numOfClasses}}
                    </div>
                    <div class="form-group">
                        <label class="col-3">Number Of Hours</label>
                        {{$coursesData->numOfHours}}
                    </div>
                    <div class="form-group">
                        <label class="col-3">Total number Of Hours</label>
                        {{$coursesData->totalnumOfHours}}
                    </div>
                    <div class="form-group">
                        <label class="col-3">Weekly Day Classes</label>
                        {{$coursesData->weeklyDayClasses}}
                    </div>
                    <div class="form-group">
                        <label class="col-3">Schedule Calender</label>
                        {{$coursesData->scheduleCalender }}
                    </div>
                    <div class="form-group">
                        <label class="col-3">Weekly Day From</label>
                        {{$coursesData->weeklyDayTime}}
                    </div>
                     <div class="form-group">
                        <label class="col-3">Weekly Days To</label>
                        {{$coursesData->weeklyDaysTime}}
                    </div>
                     <div class="form-group">
                        <label class="col-3">Free Schedule</label>
                        {{$coursesData->otherAnswer}}
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
<style type="text/css">
    .table-responsive{
        overflow-x: unset !important;
    }
</style>