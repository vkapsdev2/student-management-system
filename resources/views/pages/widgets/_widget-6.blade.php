{{-- Advance Table Widget 2 --}}
<div class="card card-custom {{ @$class }}">
   {{-- Header --}}
   <div class="card-header border-0 pt-5">
      <h3 class="card-title align-items-start flex-column">
         <span class="card-label font-weight-bolder text-dark">Personal Details</span>
      </h3>
   </div>
   {{-- Body --}}
   <div class="card-body pt-3 pb-0">
      {{-- Table --}}
      <div class="table-responsive">
         <div class="row">
            <div class="col-xl-12">
               <h3 class=" text-dark font-weight-bold mb-10"></h3>
               <div class="form-group ">
                  <label class="col-3 col-form-label">Name</label>
                  {{ $studentData->name }}
               </div>
               <div class="form-group ">
                  <label class="col-3 col-form-label">Japanese Name</label>
                  {{ $studentData->japaneseName}}
               </div>
               <div class="form-group">
                  <label class="col-3 col-form-label">Email</label>
                  {{$studentData->email}}
               </div>
               <div class="form-group">
                  <label class="col-3">Age</label>
                  {{$studentData->age}} <span>yrs</span>
               </div>
               <div class="form-group">
                  <label class="col-3"> Gender</label>
                  {{$studentData->gender}}
               </div>
               <div class="form-group">
                  <label class="col-3">Date Of Birth</label>
                  {{$studentData->dob}}
               </div>
               <div class="form-group">
                  <label class="col-3">Fax Number</label>
                  {{$studentData->faxNumber }}
               </div>
               <div class="form-group">
                  <label class="col-3">Home Number</label>
                  {{$studentData->homeNumber}}
               </div>
               <div class="form-group">
                  <label class="col-3">Mobile Number</label>
                  {{$studentData->mobileNumber }}
               </div>
               <div class="form-group">
                  <label class="col-3">Address</label>
                  {{$studentData->address}}
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="card-header border-0 pt-5">
      <h3 class="card-title align-items-start flex-column">
         <span class="card-label font-weight-bolder text-dark">Guardian Details</span>
      </h3>
   </div>
   <div class="card-body pt-3 pb-0">
      <div class="table-responsive">
         <nav>
            <div class="nav nav-tabs col-12" id="nav-tab" role="tablist">
               <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Guardian Details</a>
               <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Emergency Details</a>
               <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Academic Details</a>
            </div>
         </nav>
         <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
               <div class="card-body pt-3 pb-0">
                  {{-- Table --}}
                  <div class="table-responsive">
                     <div class="row">
                        <div class="col-xl-12">
                           <!-- <h3 class=" text-dark font-weight-bold mb-10"></h3> -->
                           <div class="form-group mt-5">
                              <label class="col-5">Guardian Name</label>
                              {{$studentData->guardianName}} 
                           </div>
                           <div class="form-group">
                              <label class="col-5">Guardian Relationship</label>
                              @if($studentData->guardianRelationship=='1')
                              <label>Self</label>
                              @elseif($studentData->guardianRelationship=='2')
                              <label>Father</label>
                              @elseif($studentData->guardianRelationship=='3')
                              <label>Mother</label>
                              @elseif($studentData->guardianRelationship=='4')
                              <label>Brother</label>
                              @elseif($studentData->guardianRelationship=='5')
                              <label>Sister</label>
                              @elseif($studentData->guardianRelationship=='6')
                              <label>Uncle</label>
                              @elseif($studentData->guardianRelationship=='7')
                              <label>Grand Father</label>
                              @elseif($studentData->guardianRelationship=='8')
                              <label>Grand Mother</label>    
                              @endif
                           </div>
                           <div class="form-group">
                              <label class="col-5">Guardian Phone Number</label>
                              {{$studentData->guardianPhoneNumber}} 
                           </div>
                           <div class="form-group">
                              <label class="col-5">Guardian Fax Number</label>
                              {{$studentData->guardianFaxNumber}} 
                           </div>
                           <div class="form-group">
                              <label class="col-5">Guardian Gender</label>
                              {{$studentData->guardianGender}} 
                           </div>
                           <div class="form-group">
                              <label class="col-5">Guardian Occupation</label>
                              {{$studentData->guardianOccupation}} 
                           </div>
                           <div class="form-group">
                              <label class="col-5">Guardian Address</label>
                              {{$studentData->guardianAddress}} 
                           </div>
                           <div class="form-group">
                              <label class="col-5">Company Name</label>
                              {{$studentData->companyName}} 
                           </div>
                           <div class="form-group">
                              <label class="col-5">Company Number</label>
                              {{$studentData->companyPhoneNumber}} 
                           </div>
                           <div class="form-group">
                              <label class="col-5">Company Address</label>
                              {{$studentData->companyAddress}} 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
               <div class="card-body pt-3 pb-0">
                  {{-- Table --}}
                  <div class="table-responsive">
                     <div class="row">
                        <div class="col-xl-12">
                           <!-- <h3 class=" text-dark font-weight-bold mb-10"></h3> -->
                           <div class="form-group mt-5">
                              <label class="col-5">Name</label>
                              {{ $studentData->emergencyName}} 
                           </div>
                           <div class="form-group">
                              <label class="col-5">Email</label>
                              {{ $studentData->emergencyEmail}}
                           </div>
                           <div class="form-group">
                              <label class="col-5">Gender</label>
                              {{ $studentData->emergencyGender}} 
                           </div>
                           <div class="form-group">
                              <label class="col-5">Relationship</label>
                              @if($studentData->emergencyRelationshipStudent=='1')
                              <label>Self</label>
                              @elseif($studentData->emergencyRelationshipStudent=='2')
                              <label>Father</label>
                              @elseif($studentData->emergencyRelationshipStudent=='3')
                              <label>Mother</label>
                              @elseif($studentData->emergencyRelationshipStudent=='4')
                              <label>Brother</label>
                              @elseif($studentData->emergencyRelationshipStudent=='5')
                              <label>Sister</label>
                              @elseif($studentData->emergencyRelationshipStudent=='6')
                              <label>Uncle</label>
                              @elseif($studentData->emergencyRelationshipStudent=='7')
                              <label>Grand Father</label>
                              @elseif($studentData->emergencyRelationshipStudent=='8')
                              <label>Grand Mother</label>    
                              @endif 
                           </div>
                           <div class="form-group">
                              <label class="col-5">Mobile Number</label>
                              {{ $studentData->emergencyCellphoneNumber }} 
                           </div>
                           <div class="form-group">
                              <label class="col-5">Home Number</label>
                              {{ $studentData->emergencyHomeNumber }} 
                           </div>
                           <div class="form-group">
                              <label class="col-5">Fax Number</label>
                              {{ $studentData->emergencyFaxNumber }} 
                           </div>
                           <div class="form-group">
                              <label class="col-5">Address</label>
                              {{ $studentData->emergencyAddress }} 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
               <div class="card-body pt-3 pb-0">
                  <div class="table-responsive">
                     <div class="row">
                        <div class="col-xl-12">
                           @if($studentAcademicData)
                           <div class="form-group mt-5">
                              <label class="col-5">School Name</label>
                              {{$studentAcademicData->schoolName}} 
                           </div>
                           <div class="form-group">
                              <label class="col-5">Standard</label>
                              @if($studentAcademicData->standard=='1')
                              Junior High School
                              @else
                              High School
                              @endif
                           </div>
                           <div class="form-group">
                              <label class="col-5">Class</label>
                              {{ $studentAcademicData->class}} 
                           </div>
                           @if($studentAcademicData->semesterName!="null")
                           @foreach(json_decode($studentAcademicData->semesterName) as $name)
                           <div class="form-group">
                              <label class="col-5">Semester Name</label>
                              {{$name}}
                           </div>
                           @endforeach
                           @else
                           <label class="col-5">Semester Name</label>
                           NA
                           @endif
                           @if($studentAcademicData->semesterGrade!="null")
                           @foreach(json_decode($studentAcademicData->semesterGrade) as $grade)
                           <div class="form-group">
                              <label class="col-5">Semester Grade</label>
                              {{$grade}}
                           </div>
                           @endforeach
                           @else
                           <label class="col-5">Semester Grade</label>
                           NA
                           @endif
                           
                           @if($studentAcademicData->semesterFile!="null")
                           @foreach(json_decode($studentAcademicData['semesterFile']) as $key => $file)
                           <div class="form-group">
                              <label class="col-5">Semester Image File</label>
                              <a href="" download>
                              <img width="50" src="{{ asset('academicStudentImages/semesterImages/'.$file)}}" alt="some image" width="">
                              </a>
                           </div>
                           @endforeach
                           @else
                           <label class="col-5">Semester Image File</label>
                           NA
                           @endif
                           @if($studentAcademicData->otherInformationName!= "null")
                           @foreach(json_decode($studentAcademicData->otherInformationName) as $otherName)
                           <div class="form-group">
                              <label class="col-6">Other Information Name </label>
                              {{ $otherName }}
                           </div>
                           @endforeach
                           @else
                           <label class="col-8">Other Information Name </label>
                           NA
                           @endif
                           @if($studentAcademicData->otherInformationFile!="null")
                           @foreach(json_decode($studentAcademicData->otherInformationFile) as $otherfile)
                           <div class="form-group">
                              <label class="col-5">Other Information Image File</label>
                              <a href="" download>
                              <img width="50" src="{{asset('academicStudentImages/otherInformationImages/'.$otherfile)}}" alt="some image" width="">
                              </a>
                           </div>
                           @endforeach
                           @else
                           <label class="col-8">Other Information Image File</label>
                           NA
                           @endif  
                           @if($studentAcademicData->schoolAchievementName!="null")
                           @foreach(json_decode($studentAcademicData->schoolAchievementName) as $AchievementName)
                           <div class="form-group">
                              <label class="col-5">School Achievement Name </label>
                              {{$AchievementName}}
                           </div>
                           @endforeach
                           @else
                           <label class="col-8">School Achievement Name </label>
                           NA
                           @endif
                           @if($studentAcademicData->schoolAchievementFile!="null")
                           @foreach(json_decode($studentAcademicData->schoolAchievementFile) as $AchievementFile)
                           <div class="form-group">
                              <label class="col-5">School Achievement Image File</label>
                              <a href="" download>
                              <img width="50" src="{{asset('academicStudentImages/schoolAchievementImages/'.$AchievementFile)}}" alt="some image" width="">
                              </a>
                           </div>
                           @endforeach
                           @else
                           <label class="col-8">School Achievement Image File</label>
                           NA
                           @endif
                           <div class="text-center m-4">
                                <a href="{{route('show-student-academic-details',['id'=>$studentData->id])}}" class="btn     btn-outline-primary">Updata Student Academic Data</a>       
                           </div>
                           @else
                           <!-- <p class="text-center text-danger">No Data</p> -->
                           <div class="text-center m-4">
                                <a href="{{route('student-academic',['id'=>$studentData->id])}}" class="text-center btn btn-outline-primary">Add Student Academic Data</a>                               
                           </div>
                           @endif
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .table-responsive{
   overflow-x: unset !important;
   }
</style>