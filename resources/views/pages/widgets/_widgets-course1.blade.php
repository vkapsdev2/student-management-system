{{-- List Widget 1 --}}

<div class="card card-custom {{ @$class }}">
    {{-- Header --}}
    <div class="card-header border-0 pt-5">
        <h3 class="card-title align-items-start flex-column">
            <span class="card-label font-weight-bolder text-dark">Profile Details</span>
        </h3>
    </div>

    {{-- Body --}}
    <div class="card-body pt-8">
        <div class="card cutomer_image_sec">
            <div class="view_profile_details card-body">
                @if ($coursesData->image)
                <img src="{{ url('/'). '/public/media/customer-image/'.$coursesData->image}}" />
                @else
                <img src="{{ url('/'). '/public/media/users/blank.png'}}"/>
                @endif
                <br/><br/>
                <h6>{{$coursesData->courseName}} </h6>
            </div>

            <div class="customer_basic_detail">
                <div class="show_email">
                    <span>Email address</span>
                    
                </div>
                <div class="show_phone">
                    <span>Phone Number</span>
                    
                </div>

<div>
    <small class="text-muted pt-4 d-block">Social Profile</small>
    <br />
    <a class="btn btn-circle btn-secondary" href="{{$coursesData->facebook_url}}" target="_blank"><i class="fab fa-facebook-f"></i></a>
    <a class="btn btn-circle btn-secondary" href="{{$coursesData->google_url}}" target="_blank"><i class="fab fa-google"></i></a>
    <a class="btn btn-circle btn-secondary" href="{{$coursesData->twitter_url}}" target="_blank"><i class="fab fa-twitter"></i></a>
    <a class="btn btn-circle btn-secondary" href="{{$coursesData->youtube_url}}" target="_blank"><i class="fab fa-youtube"></i></a>
    <a class="btn btn-circle btn-secondary" href="{{$coursesData->instagram_url}}" target="_blank"><i class="fab fa-instagram"></i></a>

</div>
</div>
</div>
    </div>
</div>
{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <style type="text/css">
        .view_profile_details.card-body {
            text-align: center;
        }
        .view_profile_details img {
            /*max-width: 90px;*/
            /*height: auto !important;*/
            /*border-radius: 50%;*/
            height: 150px;
            width: 150px;
            border-radius: 50%;
        }
        .view_profile_details h3 {
            font-size: 16px;
            margin: 10px 0px 10px;
        }
        .view_profile_details h6 {
            color: #c5baba;
        }
        .view_profile_details.card-body 
        {
            min-height: 480px;
        }
        .form-group h4 {
            padding: 0px 13px;
            font-size: 20px;
        }
        .customer_other_details {
            padding: 0px 16px;
        }
        .card.cutomer_image_sec .view_profile_details.card-body {
            min-height: auto;
        }

        .customer_basic_detail {
            padding: 15px;
            border-top: 2px solid #e4e4e4;
        }
        .customer_basic_detail .show_email {}

        .customer_basic_detail span {
            color: #c5baba;
        }
        .form-group label {
            font-size: 1rem;
            font-weight: 400;
            color: #464E5F;
            position: relative;
        }
        .form-group div.basic_dtl{
            padding-left: 20px;
        }
        span{
            position: relative;
            min-height: auto;
        }
    </style>

@endsection