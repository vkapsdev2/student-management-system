{{-- Extends layout --}}
@extends('layout.default')
@section('title','Edit Staff Profile')

{{-- Content --}}
@section('content')
<div class="container-fluid">
	<div class="col-md-10 mx-auto">
		<h2 class="text-center">Edit Student Details</h2>
		<hr>
		<form class="form-group" action="{{ route('update-staff-profile')}}" method="post">
			@csrf
			<input type="hidden" name="staff_id" value="{{ $staff_data->id }}">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Name</label>
						<input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$staff_data->name}}">
						@error('name')
		                  <span class="invalid-feedback" role="alert">
		                    <strong>{{ $message }}</strong>
		                  </span>
		                @enderror
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Japanese Name</label>
						<input type="text" class="form-control @error('japaneseName') is-invalid @enderror" name="japaneseName" value="">
						@error('japaneseName')
		                  <span class="invalid-feedback" role="alert">
		                    <strong>{{ $message }}</strong>
		                  </span>
		                @enderror
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Email</label>
						<input type="text" readonly="" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $staff_data->email }}">
						@error('email')
		                  <span class="invalid-feedback" role="alert">
		                    <strong>{{ $message }}</strong>
		                  </span>
		                @enderror
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Mobile Number</label>
						<input type="text" name="mobileNumber" class="form-control @error('mobileNumber') is-invalid @enderror" value="{{$staff_data->mobileNumber}}">
						@error('mobileNumber')
		                  <span class="invalid-feedback" role="alert">
		                    <strong>{{ $message }}</strong>
		                  </span>
		                @enderror
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Gender</label>
						<select class="form-control " name="gender" value="{{$staff_data->gender}}">
							<option value="">Select Gender</option>
							<option value="Male" class="@error('gender') is-invalid @enderror" {{$staff_data->gender=='Male'?'selected':''}}>Male  </option>
							<option value="Female" class="@error('gender') is-invalid @enderror" {{$staff_data->gender=='Female'?'selected':''}}>Female</option>
							<option value="Other" class="@error('gender') is-invalid @enderror" {{$staff_data->gender=='Other'?'selected':''}}>Other </option>
						</select>
						@error('gender')
		                  <span class="invalid-feedback" role="alert">
		                    <strong>{{ $message }}</strong>
		                  </span>
		                @enderror
					</div> 
				</div>
			</div>
			<div class="form-group">
				<label>Address</label>
				<textarea class="form-control @error('address') is-invalid @enderror" name="address" rows="5">{{$staff_data->address}}</textarea>
				@error('address')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
			</div>
			<div class="text-center mt-4">
                <input type="submit" name="" value="Update"  class="btn btn-outline-success">
            </div>
		</form>
	</div>
</div>
@endsection