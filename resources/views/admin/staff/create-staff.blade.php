@section('title','Add Staff')
{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="container-fluid">
    <div class="col-md-10 mx-auto">
        <h2 class="text-center">{{__('staff.staff_add') }}</h2>
        <hr>
        @if(session('success'))
        	<div class="alert alert-success">{{session('success')}}</div>
        @endif
        @if(session('dataMatchingError'))
	        <div class="alert alert-danger">{{session('dataMatchingError')}}</div>
        @endif
    </div>
    <form action="{{ route('save-staff') }}" method="post">
            @csrf
            <div class="form-group row">
                <label for="full_name" class="col-md-4 col-form-label text-md-right">{{ __('Full Name') }}</label>

                <div class="col-md-6">
                    <input id="full_name" type="text" class="form-control @error('full_name') is-invalid @enderror" name="full_name" value="{{ old('full_name') }}"  autocomplete="full_name">
                    <span class="text-danger" id="first-name-msg"></span>
                    @error('full_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
              <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email Address') }}</label>

              <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}">
                <span class="text-danger" id="email-msg"></span>
                @error('email')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

              <div class="col-md-6">
                <input id="gender" class="@error('gender') is-invalid @enderror" type="radio" class="" name="gender" value="Male">Male
                <input id="gender" class="@error('gender') is-invalid @enderror" type="radio" class="" name="gender" value="Female">Female
                <input id="gender" class="@error('gender') is-invalid @enderror" type="radio" class="" name="gender" value="Other">Other
                <span class="text-danger" id="phone-msg"></span>

                @error('gender')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="contact" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

              <div class="col-md-6">
                <input id="contact" type="text" class="form-control @error('contact') is-invalid @enderror" name="contact" value="{{ old('contact') }}">
                <span class="text-danger" id="phone-msg"></span>
                @error('contact')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

              <div class="col-md-6">
                <textarea id="address" class="form-control @error('address') is-invalid @enderror" name="address">{{ old('address') }}</textarea>
                <span class="text-danger" id="phone-msg"></span>
                @error('address')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>

            <div class="form-group row mb-0">
              <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-sm btn-primary" id="admin-create-pm">
                  {{ __('Create') }}
                </button>
                  <a href="{{ url()->previous() }}" class="btn btn-sm btn-danger">{{ __('Cancel') }}</a>
              </div>
            </div>
          </form>
    </form>
</div>

@endsection