{{-- Extends layout --}}
@extends('layout.default')
@section('title','Staff Profile')

{{-- Content --}}

@section('content')

    {{-- Dashboard 1 --}}
    <!-- <a class="dropdown-item" href="{{ route('logout') }}"
    onclick="event.preventDefault();
    document.getElementById('logout-form').submit();">
    {{ __('Logout') }}
    </a> -->
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
    @csrf
    </form>
<div class="row">
    <div class="col-lg-12 col-xxl-4">
        @include('pages.widgets._widget-5', ['class' => 'card-stretch gutter-b'])
    </div>
    <div class="col-lg-12 col-xxl-8"  style="overflow: hidden !important;">
        <!-- @include('pages.widgets._widget-6', ['class' => 'card-stretch gutter-b']) -->
    	<div class="card card-custom {{ @$class }}">
    		{{-- Header --}}
    		<div class="card-header border-0 pt-5">
    			<h3 class="card-title align-items-start flex-column">
    				<span class="card-label font-weight-bolder text-dark">Personal Details</span>
    			</h3>
    		</div>
    		{{-- Body --}}
    		<div class="card-body pt-3 pb-0">
    			{{-- Table --}}
    			<div class="table-responsive">
    				<div class="row">
    					<div class="col-xl-12">
    						<h3 class=" text-dark font-weight-bold mb-10"></h3>
    						<div class="form-group ">
    							<label class="col-3 col-form-label">Name</label>
    							{{ $studentData->name }}
    						</div>
    						<div class="form-group">
    							<label class="col-3 col-form-label">Email</label>
    							{{$studentData->email}}
    						</div>
    						<div class="form-group">
    							<label class="col-3">Age</label>
    							{{$studentData->age}} <span>yrs</span>
    						</div>
    						<div class="form-group">
    							<label class="col-3"> Gender</label>
    							{{$studentData->gender}}
    						</div>
    						<div class="form-group">
    							<label class="col-3">Date Of Birth</label>
    							{{$studentData->dob}}
    						</div>
    						<div class="form-group">
    							<label class="col-3">Fax Number</label>
    							{{$studentData->faxNumber }} <span>%</span>
    						</div>
    						<div class="form-group">
    							<label class="col-3">Home Number</label>
    							{{$studentData->homeNumber}}
    						</div>
    						<div class="form-group">
    							<label class="col-3">Mobile Number</label>
    							{{$studentData->mobileNumber }}
    						</div>
		                    <div class="form-group">
		                        <label class="col-3">Address</label>
		                        {{$studentData->address}}
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
@endsection
