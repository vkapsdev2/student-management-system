{{-- Extends layout --}}
@extends('layout.default')
@section('title','View Staff List')

{{-- Content --}}
@section('content')
	<div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">{{ __('staff.staff_list') }}</h3>
            </div>
            <div class="card-toolbar">
                <a href="#{{-- route('create-staff-profile') --}}" class="btn btn-primary font-weight-bolder">
                <span class="svg-icon svg-icon-md">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <circle fill="#000000" cx="9" cy="15" r="6"/>
                            <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>Create Staff</a>
                <!--end::Button-->
            </div>

        </div>
        <div class="card-body">

            <!--begin::Search Form-->
            <div class="mt-2 mb-5 mt-lg-5 mb-lg-10">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query"/>
                                    <span><i class="flaticon2-search-1 text-muted"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->
            @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
        <div class="delete_message">
        </div>
            <table class="table table-bordered table-hover" id="kt_datatable">
                <thead>
                <tr class="text-center">
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Age</th>
                    <th>Gender</th>
                    <th>Action</th>

                </tr>
                </thead>
                <tbody id="searchDataTable">
                    @forelse($staffData as $staffKey => $staffValue)
                        <tr class="text-center">
                            <td>{{$staffData->firstItem() + $staffKey}}</td>
                            <td>{{$staffValue->name}}</td>
                            <td>{{$staffValue->email}}</td>
                            <td>{{$staffValue->age}}</td>
                            <td>{{$staffValue->gender}}</td>
                            <td><a href="{{ route('edit-staff-profile',['id'=> $staffValue['id'] ]) }}" class="btn btn-secondary btn-circle mx-3" data-toggle="tooltip" data-original-title="Edit"><i class="fas fa-pencil-alt "></i></a>
                            <!-- <a href="{{url('delete-staff/'.$staffValue->id)}}" class="btn btn-danger btn-circle mx-3" data-toggle="tooltip" data-original-title="Delete"><i class="fas fa-times"> -->

                            <a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Delete" class="btn mx-3 btn-danger btn-circle" onclick="comfrimDelete('{{$staffValue->id}}','{{$staffValue->name}}')"><i class="fas fa-times"></i></a>


                            <a href="{{ route('view-staff-profile',['id'=>$staffValue['id'] ])}}" class="btn btn-primary btn-circle mx-3" data-toggle="tooltip" data-original-title="View All Detail"><i class="fas fa-eye"></i></a></td>
                        </tr>
                    @empty
                    <td class="text-center text-danger" colspan="6">No Data Found</td>
                    @endforelse
                </tbody>
            </table>
            <div class="text-center">
                {{$staffData->links()}}
            </div>
            <style type="text/css">
                .w-5{
                    display:inline;
                    width: 20px;
                }
                .flex-1{
                    display:none;
                }
            </style>
        </div>
    </div>
@endsection