{{-- Extends layout --}}
@extends('layout.default')
@section('title','Update Student Details')

{{-- Content --}}
@section('content')
<div class="container-fluid">
	<div class="col-md-10 mx-auto">
		<h2 class="text-center">Edit Student Details</h2>
		<hr>
		<form class="form-group" action="{{ route('update-student')}}" method="post">
			@csrf
			<input type="text" value="{{$studentData->id}}" name="studentId" hidden="">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Name</label>
						<input type="text" class="form-control" name="name" value="{{$studentData->name}}">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Japanese Name</label>
						<input type="text" class="form-control" name="japaneseName" value="{{$studentData->japaneseName}}">
					</div>					
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label>Age</label>
						<input type="text" class="form-control" name="age" value="{{$studentData->age}}">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Gender</label>
						<select class="form-control" name="gender" value="{{$studentData->gender}}">
							<option value="">Select</option>
							<option value="Male" {{$studentData->gender=='Male'?'selected':''}}>Male  </option>
							<option value="Female" {{$studentData->gender=='Female'?'selected':''}}>Female</option>
							<option value="Other" {{$studentData->gender=='Other'?'selected':''}}>Other </option>
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Date Of Birth</label>
						<input type="date" class="form-control" name="dob" value="{{$studentData->dob}}">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Fax Number</label>
						<input type="text" name="faxNumber" class="form-control" value="{{$studentData->faxNumber}}">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Home Number</label>
						<input type="text" class="form-control" name="homeNumber" value="{{$studentData->homeNumber}}">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Mobile Number</label>
						<input type="text" name="mobileNumber" class="form-control" value="mobileNumber">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Email</label>
						<input type="text" class="form-control" name="email" value="{{$studentData->email}}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label>Address</label>
				<textarea class="form-control" name="address" rows="5">{{$studentData->address}}</textarea>
			</div>
			<!--guardian details  -->
			<section>
				<h2 class="text-center">Guardian Details</h2>
				<hr>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Guardian Name</label>
							<input type="text" name="guardianName" class="form-control" value="{{$studentData->guardianName}}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Guardian Relationship</label>
							<select class="form-control" name="guardianRelationship" value="{{$studentData->guardianRelationship}}">
								<option value="0" {{$studentData->gender=='0'?'selected':''}}>Select Relationship</option>
	                            <option value="1" {{$studentData->guardianRelationship=='1'?'selected':''}}>Self</option>
	                            <option value="2" {{$studentData->guardianRelationship=='2'?'selected':''}}>Father</option>
	                            <option value="3" {{$studentData->guardianRelationship=='3'?'selected':''}}>Mother</option>
	                            <option value="4" {{$studentData->guardianRelationship=='4'?'selected':''}}>Brother</option>
	                            <option value="5" {{$studentData->guardianRelationship=='5'?'selected':''}}>Sister</option>
	                            <option value="6" {{$studentData->guardianRelationship=='6'?'selected':''}}>Uncle</option>
	                            <option value="7" {{$studentData->guardianRelationship=='7'?'selected':''}}>Grand Father</option>
	                            <option value="8" {{$studentData->guardianRelationship=='8'?'selected':''}}>Grand Mother</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Guardian Phone Number</label>
							<input type="text" name="guardianPhoneNumber" class="form-control" value="{{$studentData->guardianPhoneNumber}}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Guardian Fax Number</label>
							<input type="text" name="guardianFaxNumber" class="form-control" value="{{$studentData->guardianFaxNumber}}">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Guardian Gender</label>
							<select class="form-control" name="guardianGender" value="{{$studentData->guardianGender}}">
								<option value="">Select</option>
								<option value="Male" {{$studentData->guardianGender=='Male'?'selected':''}}>Male</option>
								<option value="Female" {{$studentData->guardianGender=='Female'?'selected':''}}>Female</option>
								<option value="Other" {{$studentData->guardianGender=='Other'?'selected':''}}>Other</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Guardian Occupation</label>
							<input type="text" class="form-control" name="guardianOccupation" value="{{$studentData->guardianOccupation}}">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Guardian Address</label>
					<textarea class="form-control" name="guardianAddress" rows="5">{{$studentData->guardianAddress}}</textarea>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Company Name</label>
							<input type="text" value="{{$studentData->companyName}}" name="companyName" class="form-control">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Comapny Number</label>
							<input type="text" value="{{$studentData->companyPhoneNumber}}" name="companyNumber" class="form-control">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Comapny Address</label>
					<textarea class="form-control" name="comapnyAddress" rows="5">{{$studentData->companyAddress}}</textarea>
				</div>
			</section>
			<section id="emergencyContact" class="mt-4">
                <h3 class="text-center">Emergency Contact</h3>
                <hr>
                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" value="{{$studentData->emergencyName}}" name="emergencyName" id="emergencyName" class="form-control" value="">
                            @error('emergencyName')
                                <p class="alert alert-danger mt-1">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" value="{{$studentData->emergencyEmail}}" name="emergencyEmail" id="emergencyEmail" class="form-control" value="">
                            @error('emergencyEmail')
                                <p class="alert alert-danger mt-1">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Gender</label>
                            <select class="form-control" value="{{$studentData->emergencyGender}}" name="emergencyGender" id="emergencyGender">
                                <option value="">Select</option>
                                <option value="Male" {{$studentData->emergencyGender=='Male'?'selected':''}}>Male</option>
                                <option value="Female" {{$studentData->emergencyGender=='Female'?'selected':''}}>Female</option>
                                <option value="Other" {{$studentData->emergencyGender=='Other'?'selected':''}}>Other</option>
                            </select>
                            @error('emergencyGender')
                                <p class="alert alert-danger mt-1">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Relationship</label>
                            <select class="form-control" value="{{$studentData->emergencyRelationshipStudent}}" name="emergencyRelationshipStudent" id="emergencyRelationshipStudent">
                               <option value="1" {{$studentData->emergencyRelationshipStudent=='1'?'selected':''}}>Self</option>
	                            <option value="2" {{$studentData->emergencyRelationshipStudent=='2'?'selected':''}}>Father</option>
	                            <option value="3" {{$studentData->emergencyRelationshipStudent=='3'?'selected':''}}>Mother</option>
	                            <option value="4" {{$studentData->emergencyRelationshipStudent=='4'?'selected':''}}>Brother</option>
	                            <option value="5" {{$studentData->emergencyRelationshipStudent=='5'?'selected':''}}>Sister</option>
	                            <option value="6" {{$studentData->emergencyRelationshipStudent=='6'?'selected':''}}>Uncle</option>
	                            <option value="7" {{$studentData->emergencyRelationshipStudent=='7'?'selected':''}}>Grand Father</option>
	                            <option value="8" {{$studentData->emergencyRelationshipStudent=='8'?'selected':''}}>Grand Mother</option>
                            </select>
                            @error('emergencyRelationshipStudent')
                                <p class="alert alert-danger mt-1">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Mobile Number</label>
                            <input type="text" value="{{$studentData->emergencyCellphoneNumber}}" name="emergencyCellphoneNumber" id="emergencyCellNumber" class="form-control" value="">
                            @error('emergencyCellNumber')
                                <p class="alert alert-danger mt-1">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Home Number</label>
                            <input type="text" value="{{$studentData->emergencyHomeNumber}}" name="emergencyHomeNumber" id="emergencyHomeNumber" class="form-control" value="">
                            @error('emergencyHomeNumber')
                                <p class="alert alert-danger mt-1">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Fax Number</label>
                            <input type="text" value="{{$studentData->emergencyFaxNumber}}" name="emergencyFaxNumber" id="emergencyFaxNumber" class="form-control" value="">
                            @error('emergencyFaxNumber')
                                <p class="alert alert-danger mt-1">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <textarea class="form-control" rows="5" value="" name="emergencyAddress" id="emergencyAddress" value="">{{$studentData->emergencyAddress}}</textarea>
                    @error('emergencyAddress')
                        <p class="alert alert-danger mt-1">{{$message}}</p>
                    @enderror
                </div>
            </section>

            <div class="text-center mt-4">
                <input type="submit" name="" value="Update"  class="btn btn-outline-success">
            </div>
		</form>
	</div>
</div>
@endsection