@extends('layout.default')
@section('title','Add Student')
@section('content')


<div class="container-fluid">
    <div class="col-md-10 mx-auto">
        <h2 class="text-center">Add Student Details</h2>
        <hr>
        @if(session('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>{{session('success')}}</strong>
        </div>
        @endif
        @if(session('dataMatchingError'))
        <div class="text-danger">
          {{session('dataMatchingError')}}
        </div>
        @endif
        <form action="{{ url('save-student')}}" method="post" id="save_student_details">
            @csrf
            <div class="form-group">
                <label>Name</label>
                <div class="row">
                    <div class="col-md-6 mt-2">
                        <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Enter Name in English" id="name">
                        <!-- <p id='message' class="text-danger"> -->
                        </p>
                        @error('name')
                            <p class="text-danger mt-1" id="name_error">
                                {{$message}}
                            </p>    
                        @enderror
                    </div>
                    <div class="col-md-6 mt-2">
                        <input type="text" name="japaneseName" value="{{ old('japaneseName') }}" class="form-control" placeholder="Enter Name in japanese(optional)">
                    </div>
                </div>
            </div>
            <div class="row justify-content-between">
                <div class="col-md-2 ">
                    <div class="form-group">
                        <label>Age</label>
                        <input type="text" name="age" class="form-control" value="{{ old('age') }}" id="age">
                        <!-- <p id="age_error" class="text-danger"></p> -->
                        @error('age')
                            <p class="text-danger mt-1" id="age_error1">{{$message}}</p>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Gender</label>
                        <select class="form-control" name="gender">
                            <option value="">Select</option>
                            <option value="Male" {{ old('gender') == 'Male' ? 'selected' : '' }}>Male</option>
                            <option value="Female" {{ old('gender') == 'Female' ? 'selected' : '' }}>Female</option>
                            <option value="Other" {{ old('gender') == 'Other' ? 'selected' : '' }}>Other</option>
                        </select>
                        @error('gender')
                            <p class="text-danger mt-1">{{$message}}</p>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Date Of Birth</label>
                        <input type="date" name="dob" id="dob" class="form-control" value="{{ old('dob') }}">
                        <!-- <p id="dob_error" class="text-danger"></p> -->
                        @error('dob')
                            <p class="text-danger mt-1" id="dob_error1">{{$message}}</p>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Fax Number</label>
                        <input type="text" name="faxNumber" class="form-control" id="faxNumber" value="{{ old('faxNumber') }}">
                        <!-- <p id="faxNumber_error" class="text-danger"></p> -->
                        @error('faxNumber')
                            <p class="text-danger mt-1" id="faxNumber_error1">{{$message}}</p>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Home Number</label>
                        <input type="text" name="homeNumber" class="form-control" value="{{ old('homeNumber') }}">
                        @error('homeNumber')
                            <p class="text-danger mt-1">{{$message}}</p>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Mobile Number</label>
                        <input type="text" name="mobileNumber" class="form-control" id="mobileNumber" value="{{ old('mobileNumber') }}">
                        @error('mobileNumber')
                            <p class="text-danger mt-1">{{$message}}</p>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control" id="email" value="{{ old('email') }}" autocomplete="off">
                        @error('email')
                            <p class="text-danger mt-1">{{$message}}</p>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row" hidden>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Password</label>
                        <input type="text" name="password" id="password" class="form-control">
                        @error('password')
                            <p class="text-danger mt-1">{{$message}}</p>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="confirmPassword" id="confirmPassword" class="form-control">
                        @error('confirmPassword')
                            <p class="text-danger mt-1">{{$message}}</p>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Address</label>
                <textarea class="form-control" rows="5" name="address" id="address">{{ old('address') }}</textarea>
                @error('address')
                    <p class="text-danger mt-1">{{$message}}</p>
                @enderror
            </div>

            <!--guardian details  -->

            <div id="guardindetails" class="mt-4">
                <div class="form-group">
                </div>
                <h3 class="text-center">Guardian Details</h3>
                <hr>
                <div class="form-check-inline mb-5">
                        <label class="form-check-label">
                            <input type="checkbox" name="checkForGuardian" value="on"
                             {{ old('checkForGuardian')=='on'? 'checked' :" "}}
                            class="form-check-input" id="sameasstudent" onclick="check()">
                        Same As Student</label>
                    </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Guardian Name</label>
                            <input type="text" name="guardianName" id="guardianName" class="form-control" value="{{ old('guardianName') }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Guardian Relationship</label>
                        <select class="form-control" name="guardianRelationship">
                            <option value="0">Select Relationship</option>
                            <option value="1" {{ old('guardianRelationship') == '1' ? 'selected' : '' }}>Self</option>
                            <option value="2" {{ old('guardianRelationship') == '2' ? 'selected' : '' }}>Father</option>
                            <option value="3" {{ old('guardianRelationship') == '3' ? 'selected' : '' }}>Mother</option>
                            <option value="4" {{ old('guardianRelationship') == '4' ? 'selected' : '' }}>Brother</option>
                            <option value="5" {{ old('guardianRelationship') == '5' ? 'selected' : '' }}>Sister</option>
                            <option value="6" {{ old('guardianRelationship') == '6' ? 'selected' : '' }}>Uncle</option>
                            <option value="7" {{ old('guardianRelationship') == '7' ? 'selected' : '' }}>Grand Father</option>
                            <option value="8" {{ old('guardianRelationship') == '8' ? 'selected' : '' }}>Grand Mother</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Guardian Phone Number</label>
                            <input type="text" name="guardianPhoneNumber" class="form-control" id="guardianPhoneNumber" value="{{ old('guardianPhoneNumber') }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Guardian Fax Number</label>
                        <input type="text" class="form-control" name="guardianFaxNumber"  id="guardianFaxNumber" value="{{ old('guardianFaxNumber') }}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Guardian Gender</label>
                            <select class="form-control" name="guardianGender">
                                <option value="0">Select</option>
                                <option value="Male" {{ old('guardianGender') == 'Male' ? 'selected' : '' }}>Male</option>
                                <option value="Female"{{ old('guardianGender') == 'Female' ? 'selected' : '' }}>Female</option>
                                <option value="Other"{{ old('guardianGender') == 'Other' ? 'selected' : '' }}>Other</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Guardian Occupation</label>
                            <input type="text" name="guardianOccupation" id="guardianOccupation" class="form-control" value="{{ old('guardianOccupation') }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Guardian Address</label>
                    <textarea class="form-control" rows="5" name="guardianAddress" id="guardianAddress" value="">{{ old('guardianAddress') }}</textarea>
                </div>
                <div class="row">
                    <div class="col-md-6">
                         <div class="form-group">
                            <label>Company Name</label>
                            <input type="text" name="companyName" id="companyName" class="form-control" value="{{ old('companyName') }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                         <div class="form-group">
                            <label>Company Number</label>
                            <input type="text" name="companyNumber" class="form-control" id="companyNumber" value="{{ old('companyNumber') }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>company Address</label>
                    <textarea class="form-control" rows="5" name="companyAddress" id="companyAddress" value="">{{ old('companyAddress') }}</textarea>
                </div>
            </div>



            <!-- emergency contact but not same as the student and parent details -->
            <section id="emergencyContact" class="mt-4">
                <h3 class="text-center">Emergency Contact</h3>
                <hr>
                <b>NOTE</b><span class="text-danger">:-Emergency details can't be same as Student details.</span>
                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="emergencyName" id="emergencyName" class="form-control" value="{{ old('emergencyName') }}">
                            @error('emergencyName')
                                <p class="text-danger mt-1">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="emergencyEmail" id="emergencyEmail" class="form-control" value="{{ old('emergencyEmail') }}">
                            @error('emergencyEmail')
                                <p class="text-danger mt-1">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Gender</label>
                            <select class="form-control" name="emergencyGender" id="emergencyGender">
                                <option value="">Select</option>
                                <option value="Male" {{ old('emergencyGender') == 'Male' ? 'selected' : '' }}>Male</option>
                                <option value="Female" {{ old('emergencyGender') == 'Femal' ? 'selected' : '' }}>Female</option>
                                <option value="Other" {{ old('emergencyGender') == 'Other' ? 'selected' : '' }}>Other</option>
                            </select>
                            @error('emergencyGender')
                                <p class="text-danger mt-1">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Relationship</label>
                            <select class="form-control" name="emergencyRelationshipStudent" id="emergencyRelationshipStudent">
                                <option value="">Select Relationship</option>
                                <option value="1" {{ old('emergencyRelationshipStudent') == '1' ? 'selected' : '' }}>Self</option>
                                <option value="2" {{ old('emergencyRelationshipStudent') == '2' ? 'selected' : '' }}>Father</option>
                                <option value="3" {{ old('emergencyRelationshipStudent') == '3' ? 'selected' : '' }}>Mother</option>
                                <option value="4" {{ old('emergencyRelationshipStudent') == '4' ? 'selected' : '' }}>Brother</option>
                                <option value="5" {{ old('emergencyRelationshipStudent') == '5' ? 'selected' : '' }}>Sister</option>
                                <option value="6" {{ old('emergencyRelationshipStudent') == '6' ? 'selected' : '' }}>Uncle</option>
                                <option value="7" {{ old('emergencyRelationshipStudent') == '7' ? 'selected' : '' }}>Grand Father</option>
                                <option value="8" {{ old('emergencyRelationshipStudent') == '8' ? 'selected' : '' }}>Grand Mother</option>
                            </select>
                            @error('emergencyRelationshipStudent')
                                <p class="text-danger mt-1">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Mobile Number</label>
                            <input type="text" name="emergencyCellNumber" id="emergencyCellNumber" class="form-control" value="{{ old('emergencyCellNumber') }}">
                            @error('emergencyCellNumber')
                                <p class="text-danger mt-1">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Home Number</label>
                            <input type="text" name="emergencyHomeNumber" id="emergencyHomeNumber" class="form-control" value="{{ old('emergencyHomeNumber') }}">
                            @error('emergencyHomeNumber')
                                <p class="text-danger mt-1">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Fax Number</label>
                            <input type="text" name="emergencyFaxNumber" id="emergencyFaxNumber" class="form-control" value="{{ old('emergencyFaxNumber') }}">
                            @error('emergencyFaxNumber')
                                <p class="text-danger mt-1">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <textarea class="form-control" rows="5" name="emergencyAddress" id="emergencyAddress" value="">{{ old('emergencyAddress') }}</textarea>
                    @error('emergencyAddress')
                        <p class="text-danger mt-1">{{$message}}</p>
                    @enderror
                </div>
            </section>

            <div class="text-center mt-4">
                <input type="submit" name="" value="Add Data"  class="btn btn-outline-success" id="submit">
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    function check(){
        var checkbox=document.getElementById('sameasstudent');
        var name=document.getElementById('name').value;
        var guardianName=document.getElementById('guardianName');
        var mobileNumber=document.getElementById('mobileNumber').value;
        var guardianPhoneNumber=document.getElementById('guardianPhoneNumber');
        var faxNumber=document.getElementById('faxNumber').value;
        var guardianFaxNumber=document.getElementById('guardianFaxNumber');
        if(checkbox.checked==true)
        {
            guardianName.value=name;
            guardianPhoneNumber.value=mobileNumber;
            guardianFaxNumber.value=faxNumber;
        }
        if(checkbox.checked==false)
        {
            guardianName.value=" ";
            guardianPhoneNumber.value=" ";
            guardianFaxNumber.value=" ";
        }
    }
      
</script>
<style type="text/css">
label.error.fail-alert {
color:red;
}
</style>

@endsection
