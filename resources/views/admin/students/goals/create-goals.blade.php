@section('title','Add Goals')
{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="container-fluid">
    <div class="col-md-10 mx-auto">
        <h2 class="text-center">Add Your Goal</h2>
        <hr>
        @if(session('success'))
        	<div class="alert alert-success">{{session('success')}}</div>
        @endif
        @if(session('dataMatchingError'))
	        <div class="alert alert-danger">{{session('dataMatchingError')}}</div>
        @endif
    </div>
    <form action="{{-- route('') --}}" method="post">
            @csrf
            <div class="form-group row">
                <label for="student_id" class="col-md-4 col-form-label text-md-right">{{ __('Goal Name') }}</label>

                <div class="col-md-6">
                     <select class="form-control" name="student">
                        <option value="">Select Student Name</option>
                        <option class="form-control" name="student_id" value=""></option>
                        <option value="">Students 1</option>
                        <option value="">Students 1</option>
                        <option value="">Students 1</option>
                        <option value="">Students 1</option>
                        
                     </select>
                    @error('student_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="Goal_name" class="col-md-4 col-form-label text-md-right">{{ __('Goal Name') }}</label>

                <div class="col-md-6">
                    <input id="Goal_name" type="text" class="form-control @error('Goal_name') is-invalid @enderror" name="Goal_name" value="{{ old('Goal_name') }}"  autocomplete="">
                    <span class="text-danger" id="first-name-msg"></span>
                    @error('Goal_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
              <label for="goalDeadline" class="col-md-4 col-form-label text-md-right">{{ __('Goal Deadline ') }}</label>

              <div class="col-md-6">
                <input id="goalDeadline" type="date" class="form-control @error('goalDeadline') is-invalid @enderror" name="goalDeadline" value="{{ old('goalDeadline') }}">
                <span class="text-danger" id="goalDeadline"></span>
                @error('goalDeadline')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="form-group row mb-0">
              <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-sm btn-primary" id="admin-create-pm">
                  {{ __('Create') }}
                </button>
                  <a href="{{ url()->previous() }}" class="btn btn-sm btn-danger">{{ __('Cancel') }}</a>
              </div>
            </div>
          </form>
    </form>
</div>

@endsection