@extends('layout.default')
@section('title','Update Academic Details')
@section('content')

<div class="container-fluid">
	<h2 class="text-center">Update Academic Information</h2>
	<form action="{{ route('update-academic-student-details')}}" method="post" id="" enctype=multipart/form-data id="update-academic_information	">
		@csrf
		@if(session('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>{{session('success')}}</strong>
        </div>
        @endif
		<section id="schoolDetails">
			<h3 class="mt-5">School Information</h3>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>{{__('Student Name')}}</label>
						<!-- <input type="text" name="schoolName" id="schoolName" class="form-control" value="{{-- old('schoolName') --}}"> -->
						<select class="form-control" name="studentId">
								<option class="form-control" value="{{$studentData['id']}}">{{ $studentData['name'] }}</option>
						</select>
					</div>
				</div>
				<div class="col-md-5">
					<div class="form-group">
						<label>School Name</label>
						<input type="text" name="schoolName" id="schoolName" class="form-control" value="{{$academicData->schoolName}}">
						@error('schoolName')
							<div class="alert alert-danger">
								{{$message}}
							</div>
						@enderror
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Select Standard</label>
						<select class="form-control" name="studentStandard">
							<option value="1" {{($academicData->standard=='1') ?'selected':""}}>Junior High School</option>
							<option value="2" {{($academicData->standard=='2') ?'selected':""}}>High School</option>
						</select>
						@error('studentStandard')
							<div class="alert alert-danger">
								{{$message}}
							</div>
						@enderror
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Select Class</label>
						<select name="studentClass" id="studentClass" class="form-control">
							<option value="">Select</option>
							<option value="6" {{($academicData->class=='6') ?'selected':""}}>6</option>
							<option value="7" {{($academicData->class=='7') ?'selected':""}}>7</option>
							<option value="8" {{($academicData->class=='8') ?'selected':""}}>8</option>
							<option value="9" {{($academicData->class=='9') ?'selected':""}}>9</option>
							<option value="10" {{($academicData->class=='10') ?'selected':""}}>10</option>
							<option value="11" {{($academicData->class=='11') ?'selected':""}}>11</option>
							<option value="12" {{($academicData->class=='12') ?'selected':""}}>12</option>
						</select>
						@error('studentClass')
							<div class="alert alert-danger">
								{{$message}}
							</div>
						@enderror
					</div>
				</div>
			</div>
		</section>
		<!-- Semester Details -->
		
		<section id="studentSemester" class="my-5" >
			<h3 class="text-secondary">Semester Information</h3>
			<hr>
			@for($i=0;$i<$semesterCount;$i++)
			<div class="row" id="semester">
				<div class="col-md-4" >
					<div class="form-group">
						<label>Semester</label>
						<input type="text" name="studentSemesterName[]" id="studentSemesterName" class="form-control" value="{{$academicData->semesterName[$i]}}">
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>Grade</label>
						<input type="text" name="studentSemesterGrade[]" id="studentSemesterGrade" class="form-control" value="{{$academicData->semesterGrade[$i]}}">
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label class="col-md-12">Image</label>
						@if($academicData->semesterFile[$i])
						<img  width="30" src="{{asset('academicStudentImages/semesterImages/'.$academicData->semesterFile[$i])}}" alt="some image" width="" name="studentSemestersImage[]">
						@else
						no image
						@endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Upload File</label>
						<input type="file" name="studentSemesterFile[]" id="studentSemesterFile" class="semesterFileCheck form-control-file" value="{{$academicData->semesterFile[$i]}}">
					</div>
				</div>
				<div class="col-md-1">
					<button  class="btn btn-outline-danger" id="deleteSemester" type="button"><i class="fa fa-trash"></i></button>
				</div>
			</div>
			@endfor
			<div id="addMoreSemester">

			</div>
			@error('studentSemesterName')
			<div class="alert alert-danger col-md-4 mx-auto" id="semester_error">
				{{$message}}
			</div>
			@enderror	
			<button type="button" class="btn btn-primary" id="addSemester">+ Add Semester</button>
		</section>
		<!-- Any Other Information-->
		<section id="anyOtherInformation" class="my-5">
			<h3>Any Other Information</h3>
			<hr>
			@for($i=0;$i<$otherCount;$i++)
			<div class="row " id="information">
				<div class="col-md-4">
					<div class="form-group">
						<label>Other Information</label>
						<input type="text" name="otherInformationName[]" id="otherInformationName" class="form-control" value="{{$academicData->otherInformationName[$i]}}">
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label class="col-md-12">Image</label>
						@if($academicData->otherInformationFile[$i])
						<img width="30" src="{{asset('academicStudentImages/otherInformationImages/'.$academicData->otherInformationFile[$i])}}" alt="some image" width="" name="otherInformationImage[]">
						@else
							no image
						@endif
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Upload File</label>
						<input type="file" name="otherInformatonFile[]" id="otherInformatonFile" class="form-control-file" value="{{$academicData->otherInformationFile[$i]}}">
					</div>
				</div>
				<div class="col-md-1">
					<button type="button"  class="btn btn-outline-danger" id="deleteOtherInformation"><i class="fa fa-trash"></i></button>
				</div>
			</div>
			@endfor
			<div id="addMoreOtherInformation">

			</div>
			<button type="button" class="btn btn-primary" id="addOtherInformation">+ Add Other Infomation</button>
		</section>

		<!-- school achievement -->
		<section id="schoolAchievement" class="my-5">
			<h3>School Achievement</h3>
			<hr>
			@for($i=0;$i<$schoolCount;$i++)
			<div class="row " id="achievement">
				<div class="col-md-4">
					<div class="form-group">
						<label>School Achievement</label>
						<input type="text"name="schoolAchievementName[]" id="schoolAchievementName" class="form-control" value="{{$academicData->schoolAchievementName[$i]}}">
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label class="col-md-12">image</label>
						<img width="30" src="{{asset('academicStudentImages/schoolAchievementImages/'.$academicData->schoolAchievementFile[$i])}}" alt="some image"  name="schoolAchievementImage[]">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Upload File</label>
						<input type="file" name="schoolAchievementFile[]" id="schoolAchievementFile" class="form-control-file" value="{{$academicData->schoolAchievementFile[$i]}}">
					</div>
				</div>
				<div class="col-md-1">
					<button  class="btn btn-outline-danger" id="deleteMoreAchievement"><i class="fa fa-trash"></i></button>
				</div>
			</div>
			@endfor
			<div id="addMoreSchoolAchievement"></div>
			<button type="button" class="btn btn-lg btn-primary" id="addSchoolAchievement">+ Add School Achievement</button>
		</section>
		<div class="text-center">
			<input type="submit" class="btn btn-outline-success" name="submit"  value="Update Data">
		</div>
	</form>
</div>
<script type="text/javascript">
</script>
@endsection
