@extends('layout.default')
@section('title','Student Academic Details')
@section('content')

<div class="container-fluid">
	<h2 class="text-center">Add Academic Information</h2>
	<form action="{{ route('save-academic-student-details')}}" method="post" id="academic_information" enctype=multipart/form-data>
		@csrf
		@if(session('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>{{session('success')}}</strong>
        </div>
        @endif
		<section id="schoolDetails">
			<h3>School Information</h3>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>{{__('Student Name')}}</label>
						<!-- <input type="text" name="schoolName" id="schoolName" class="form-control" value="{{-- old('schoolName') --}}"> -->
						@if($urlId)
							<select class="form-control">
							@foreach($urlId as $urlKey => $urlIdValue)
								<option value="{{$urlIdValue['id']}}">{{$urlIdValue['name']}}</option>
							@endforeach	
							</select>
							
						@else
						<select class="form-control" name="studentId" id="searchSelect">
							<option value="">Select student</option>
							@forelse($studentData as $studentDataKey => $studentDataValue)
								<option class="form-control" value="{{ $studentDataValue['id'] }}" {{old('studentId')==$studentDataValue['id'] ? 'selected':'' }}>{{ $studentDataValue['name'] }}</option>
							@empty
								<option value="">No Students</option>
							@endforelse
						</select>
						@endif
					</div>
				</div>
				<div class="col-md-5">
					<div class="form-group">
						<label>School Name</label>
						<input type="text" name="schoolName" id="schoolName" class="form-control" value="{{old('schoolName')}}">
						@error('schoolName')
							<div class="alert alert-danger">
								{{$message}}
							</div>
						@enderror
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Select Standard</label>
						<select class="form-control" name="studentStandard">
							<option value="">Select</option>
							<option value="1" {{old('studentStandard')=='1' ? 'selected':''}}>Junior High School</option>
							<option value="2" {{old('studentStandard')=='2' ? 'selected':''}}>High School</option>
						</select>
						@error('studentStandard')
							<div class="alert alert-danger">
								{{$message}}
							</div>
						@enderror
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Select Class</label>
						<select name="studentClass" id="studentClass" class="form-control">
							<option value="">Select</option>
							<option value="6" {{old('studentClass')=='6' ? 'selected':''}}>6</option>
							<option value="7" {{old('studentClass')=='7' ? 'selected':''}}>7</option>
							<option value="8" {{old('studentClass')=='8' ? 'selected':''}}>8</option>
							<option value="9" {{old('studentClass')=='9' ? 'selected':''}}>9</option>
							<option value="10" {{old('studentClass')=='10' ? 'selected':''}}>10</option>
							<option value="11" {{old('studentClass')=='11' ? 'selected':''}}>11</option>
							<option value="12" {{old('studentClass')=='12' ? 'selected':''}}>12</option>
						</select>
						@error('studentClass')
							<div class="alert alert-danger">
								{{$message}}
							</div>
						@enderror
					</div>
				</div>
			</div>
		</section>
		<!-- Semester Details -->
		<section id="studentSemester" class="my-5" >
			<h3 class="text-secondary">Semester Information</h3>
			<hr>
			<div class="row" id="semester">
				<div class="col-md-4" >
					<div class="form-group">
						<label>Semester</label>
						<input type="text" name="studentSemesterName[]" id="studentSemesterName" class="form-control">
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>Grade</label>
						<input type="text" name="studentSemesterGrade[]" id="studentSemesterGrade" class="form-control">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Upload File</label>
						<input type="file" name="studentSemesterFile[]" id="studentSemesterFile" class="semesterFileCheck form-control-file" value="{{old("studentSemesterFile")}}">
					</div>
				</div>
				<div class="col-md-1">
					<!-- <button  class="btn btn-outline-danger" id="deleteSemester" type="button"><i class="fa fa-trash"></i></button> -->
				</div>
			</div>
			<div id="addMoreSemester">

			</div>
			@error('studentSemesterName')
			<div class="alert alert-danger col-md-4 mx-auto" id="semester_error">
				{{$message}}
			</div>
			@enderror	
			<button type="button" class="btn btn-primary" id="addSemester">+ Add Semester</button>
		</section>
		<!-- Any Other Information-->
		<section id="anyOtherInformation" class="my-5">
			<h3>Any Other Information</h3>
			<hr>
			<div id="addMoreOtherInformation">

			</div>
			<button type="button" class="btn btn-primary" id="addOtherInformation">+ Add Other Infomation</button>
		</section>

		<!-- school achievement -->
		<section id="schoolAchievement" class="my-5">
			<h3>School Achievement</h3>
			<hr>
			<div id="addMoreSchoolAchievement"></div>
			<button type="button" class="btn btn-lg btn-primary" id="addSchoolAchievement">+ Add School Achievement</button>
		</section>
		<div class="text-center">
			<input type="submit" class="btn btn-outline-success" name="submit"  value="Save">
		</div>
	</form>
</div>
<script type="text/javascript">
</script>
@endsection
