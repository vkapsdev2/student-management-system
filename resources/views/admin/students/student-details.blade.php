{{-- Extends layout --}}
@extends('layout.default')
@section('title','Student Profile')


{{-- Content --}}

@section('content')

    {{-- Dashboard 1 --}}
    <!-- <a class="dropdown-item" href="{{ route('logout') }}"
    onclick="event.preventDefault();
    document.getElementById('logout-form').submit();">
    {{ __('Logout') }}
    </a> -->
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
    @csrf
    </form>
    <div class="row">
            <div class="col-md-5">
                @include('pages.widgets._widget-5', ['class' => 'card-stretch gutter-b'])
            </div>
               <div class="col-md-7">
                @include('pages.widgets._widget-6', ['class' => 'card-stretch gutter-b'])
            </div>
    </div>





@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
@endsection
