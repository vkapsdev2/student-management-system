{{-- Extends layout --}}
@extends('layout.default')
@section('title','My Profile')


{{-- Content --}}

@section('content')
 <div class="row">
            <div class="col-lg-12 col-xxl-4">
	<div class="card card-custom {{ @$class }}">
    {{-- Header --}}
    <div class="card-header border-0 pt-5">
        <h3 class="card-title align-items-start flex-column">
            <span class="card-label font-weight-bolder text-dark">Admin Profile Details</span>
        </h3>
    </div>

    {{-- Body --}}
    <div class="card-body pt-8">
        <div class="card cutomer_image_sec">
            <div class="view_profile_details card-body">
                @if ($studentData->image)
                <img src="{{ url('/'). '/public/media/customer-image/'.$studentData->image}}" />
                @else
                <img src="{{ url('/'). '/public/media/users/blank.png'}}"/>
                @endif
                <br/><br/>
                <h6>{{$studentData->name}} </h6>
            </div>

            <div class="customer_basic_detail">
                <div class="show_email">
                    <span>Email address</span>
                    <p>{{$studentData->email}}</p>
                </div>
                <div class="show_phone">
                    <span>Phone Number</span>
                    <p>{{$studentData->mobileNumber }}</p>
                </div>

<div>
    <small class="text-muted pt-4 d-block">Social Profile</small>
    <br />
    <a class="btn btn-circle btn-secondary" href="{{$studentData->facebook_url}}" target="_blank"><i class="fab fa-facebook-f"></i></a>
    <a class="btn btn-circle btn-secondary" href="{{$studentData->google_url}}" target="_blank"><i class="fab fa-google"></i></a>
    <a class="btn btn-circle btn-secondary" href="{{$studentData->twitter_url}}" target="_blank"><i class="fab fa-twitter"></i></a>
    <a class="btn btn-circle btn-secondary" href="{{$studentData->youtube_url}}" target="_blank"><i class="fab fa-youtube"></i></a>
    <a class="btn btn-circle btn-secondary" href="{{$studentData->instagram_url}}" target="_blank"><i class="fab fa-instagram"></i></a>

</div>
</div>
</div>
    </div>
</div>
</div>
 <div class="col-lg-12 col-xxl-8">
    	
<div class="card card-custom {{ @$class }}">
    {{-- Header --}}
    <div class="card-header border-0 pt-5">
        <h3 class="card-title align-items-start flex-column">
            <span class="card-label font-weight-bolder text-dark">Personal Details</span>
        </h3>
    </div>

    {{-- Body --}}
    <div class="card-body pt-3 pb-0">
        {{-- Table --}}
        <div class="table-responsive">
            <div class="row">
                <div class="col-xl-12">
                    <h3 class=" text-dark font-weight-bold mb-10"></h3>
                    <div class="form-group ">
                        <label class="col-3 col-form-label">Name</label>
                        {{ $studentData->name }}
                    </div>
                    <div class="form-group">
                        <label class="col-3 col-form-label">Email</label>
                         {{$studentData->email}}
                    </div>
                    <div class="form-group">
                        <label class="col-3">Age</label>
                        {{$studentData->age}} <span>yrs</span>
                    </div>
                    <div class="form-group">
                        <label class="col-3"> Gender</label>
                        {{$studentData->gender}}
                    </div>
                    <div class="form-group">
                        <label class="col-3">Date Of Birth</label>
                        {{$studentData->dob}}
                    </div>
                    <div class="form-group">
                        <label class="col-3">Fax Number</label>
                        {{$studentData->faxNumber }} <span>%</span>
                    </div>
                    <div class="form-group">
                        <label class="col-3">Home Number</label>
                        {{$studentData->homeNumber}}
                    </div>
                    <div class="form-group">
                        <label class="col-3">Mobile Number</label>
                        {{$studentData->mobileNumber }}
                    </div>
                    <div class="form-group">
                        <label class="col-3">Address</label>
                        {{$studentData->address}}
                    </div>

                </div>
            </div>
        </div>
    </div>     
</div>
<style type="text/css">
    .table-responsive{
        overflow-x: unset !important;
    }
</style>           
    </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <style type="text/css">
        .view_profile_details.card-body {
            text-align: center;
        }
        .view_profile_details img {
            /*max-width: 90px;*/
            /*height: auto !important;*/
            /*border-radius: 50%;*/
            height: 150px;
            width: 150px;
            border-radius: 50%;
        }
        .view_profile_details h3 {
            font-size: 16px;
            margin: 10px 0px 10px;
        }
        .view_profile_details h6 {
            color: #c5baba;
        }
        .view_profile_details.card-body 
        {
            min-height: 480px;
        }
        .form-group h4 {
            padding: 0px 13px;
            font-size: 20px;
        }
        .customer_other_details {
            padding: 0px 16px;
        }
        .card.cutomer_image_sec .view_profile_details.card-body {
            min-height: auto;
        }

        .customer_basic_detail {
            padding: 15px;
            border-top: 2px solid #e4e4e4;
        }
        .customer_basic_detail .show_email {}

        .customer_basic_detail span {
            color: #c5baba;
        }
        .form-group label {
            font-size: 1rem;
            font-weight: 400;
            color: #464E5F;
            position: relative;
        }
        .form-group div.basic_dtl{
            padding-left: 20px;
        }
        span{
            position: relative;
            min-height: auto;
        }
    </style>

@endsection