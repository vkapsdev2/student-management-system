{{-- Extends layout --}}
@extends('layout.default')
@section('title','Student Profile')


{{-- Content --}}

@section('content')

    {{-- Dashboard 1 --}}
    <div class="row">
        <div class="col-lg-12 col-xxl-12">
            @include('pages.widgets._widgets-course2', ['class' => 'card-stretch gutter-b'])
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
@endsection
