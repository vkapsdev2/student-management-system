@section('title','Add Course')
@extends('layout.default')
@section('content')
<div class="container-fluid" >
   <div class="col-md-10 mx-auto addMoreCourse">
      <h2 class="text-center">Add Schedule Details</h2>
      <hr>
      @if(session('success'))
      <div class="alert alert-success alert-dismissible">
         <button type="button" class="close" data-dismiss="alert">&times;</button>
         <strong>{{session('success')}}</strong>
      </div>
      @endif
      @if(session('dataMatchingError'))
      <div class="alert alert-danger">
         {{session('dataMatchingError')}}
      </div>
      @endif
      <form action="{{ route('save-course-information')}}" method="post" name="submitform" >
         @csrf
         <div class="form-group">
            <div class="row">
               <div class="col-md-12">
                  <div class="form-group">
                     <label>{{__('Student Name')}}</label>
                     <!-- <input type="text" name="schoolName" id="schoolName" class="form-control" value="{{-- old('schoolName') --}}"> -->
                     <select class="form-control" name="student">
                        <option value="0">Select Student Name</option>
                        @forelse($studentData as $studentDataKey => $studentDataValue)
                        <option class="form-control" name="student_id" value="{{ $studentDataValue['id'] }}">{{ $studentDataValue['name'] }}</option>
                        @empty
                        <option value="">No Students</option>
                        @endforelse
                     </select>
                     @error('student_id')
                     <p class="alert alert-danger mt-1">{{$message}}</p>
                     @enderror
                  </div>
               </div>
               <!-- <div class="col-md-4 mt-8 ">
                  <button type="button" class="btn btn-outline-dark btn-block float-right" id="addMoreCourseSchedule">Add Course Schedule <i class="fas fa-plus-square"></i></button>
               </div> -->
            </div>
         </div>
         <div class="section">
            <div class="form-group">
               <div class="row">
                  <div class="col-md-4 mt-2">
                     <label>Course Name</label>
                     <select class="form-control" name="courseName">
                        <option value="">Select</option>
                        <option value="0">Select Course Name</option>
                        @forelse($coursesData as $coursesDataKey => $coursesDataValue)
                        <option class="form-control" name="student_id" value="{{ $coursesDataValue['course_id'] }}">{{ $coursesDataValue['course_name'] }}</option>
                        @empty
                        <option value="">No Students</option>
                        @endforelse
                     </select>
                     @error('courseName')
                     <p class="alert alert-danger mt-1">{{$message}}</p>
                     @enderror
                  </div>
                  <div class="col-md-4 mt-2">
                     <label>Teacher Incharge</label>
                     <select class="form-control" name="staffIncharge">
                        <option value="0">Select Teacher Incharge</option>
                        @forelse($staffData as $staffDataKey => $staffDataValue)
                        <option class="form-control" name="student_id" value="{{ $staffDataValue['id'] }}">{{ $staffDataValue['name'] }}</option>
                        @empty
                        <option value="">No Staff</option>
                        @endforelse
                     </select>
                     @error('staffIncharge')
                     <p class="alert alert-danger mt-1">{{$message}}</p>
                     @enderror
                  </div>
                  <div class="col-md-4 mt-2">
                     <label>Supporting Teacher </label>
                     <select class="form-control" name="supportStaff" value="{{old('gender')}}">
                        <option value="0">Select Supporting Staff</option>
                        @forelse($staffData as $staffDataKey => $staffDataValue)
                        <option class="form-control" name="student_id" value="{{ $staffDataValue['id'] }}">{{ $staffDataValue['name'] }}</option>
                        @empty
                        <option value="">No Staff</option>
                        @endforelse
                     </select>
                     @error('supportStaff')
                     <p class="alert alert-danger mt-1">{{$message}}</p>
                     @enderror
                  </div>
               </div>
               <hr>
               <h2 class="text-center">Select Classes Schedule</h2>
               <hr>
               <div class="form-group">
                  <div class="row">
                     <div class="col-md-8 mt-4">
                        <label>Types of Classes</label>
                        <select class="form-control typeOfClasses" name="typeOfClasses" id="typeOfClasses">
                           <option value="" >Select</option>
                           <option value="monthly" {{ old('typeOfClasses') == 'Monthly' ? 'selected' : '' }}>Monthly</option>
                           <option value="weekly" {{ old('typeOfClasses') == 'Weekly' ? 'selected' : '' }}>Weekly</option>
                        </select>
                        @error('typeOfClasses')
                        <p class="alert alert-danger mt-1">{{$message}}</p>
                        @enderror
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-4 mt-2">
                        <label>Total Number of Classes:</label>
                        <input type="text" name="numOfClasses" id="numOfClasses" value="{{ old('numOfClasses') }}" class="form-control" onkeyup="onkeyupsum()" placeholder="Enter Number of Classes" autocomplete="off">
                        @error('numOfClasses')
                        <p class="alert alert-danger mt-1">{{$message}}</p>
                        @enderror
                     </div>
                     <div class="col-md-4 mt-2">
                        <label>Number of HoursPerDay:</label>
                        <input type="text" name="numOfHours" value="{{ old('numOfHours') }}" class="form-control" onkeyup="onkeyupsum()" placeholder="Enter Number of HoursPerDay" id="numOfHours" autocomplete="off">
                        @error('numOfHours')
                        <p class="alert alert-danger mt-1">{{$message}}</p>
                        @enderror
                     </div>
                     <div class="col-md-4 mt-2">
                        <label>Total Hours:</label>
                        <input type="number" readonly="on" name="totalnumOfHours" id="tHours" class="form-control" placeholder="Total Calculated Hours" autocomplete="off" value="{{old('totalnumOfHours')}}">
                        @error('totalnumOfHours')
                        <p class="alert alert-danger mt-1">{{$message}}</p>
                        @enderror
                     </div>
                  </div>
               </div>
               <hr>
               <h2 class="text-center">Select Course Durations</h2>
               <hr>
               <div class="form-group">
                  <div class="row">
                     <div class="col-md-4 mt-4">
                        <label>Total Course Duration</label>
                         <select class="form-control" name="courseName">
                        <option value="0">Select Course Duration</option>
                        @forelse($coursesData as $coursesDataKey => $coursesDataValue)
                        <option class="form-control" name="student_id" value="{{ $coursesDataValue['course_id'] }}">{{ $coursesDataValue['course_duration'] }}</option>
                        @empty
                        <option value="">No Students</option>
                        @endforelse
                     </select>
                        @error('typeOfClasses')
                        <p class="alert alert-danger mt-1">{{$message}}</p>
                        @enderror
                     </div>
                     <div class="col-md-4 mt-4">
                        <label>Course Starting Date:</label>
                        <input type="text" name="courseStartDate" id="courseStartDate" class="form-control" value="{{ old('courseStartDate') }}">
                        @error('courseStartDate')
                        <p class="alert alert-danger mt-1">{{$message}}</p>
                        @enderror
                     </div>
                     <div class="col-md-4 mt-4">
                        <label>Course Ending Date:</label>
                        <input type="text" name="courseEndDate" id="courseEndDate" class="form-control" value="{{ old('courseEndDate') }}">
                        @error('courseEndDate')
                        <p class="alert alert-danger mt-1">{{$message}}</p>
                        @enderror
                     </div>
                  </div>
               </div>
               <hr>
               <h2 class="text-center">Select Classes Schedule</h2>
               <hr>
               <div class="form-check" style="font-size: 15px;">
                  <input type="radio" class="form-check-input sh" id="setsch" value="setsch" name="setsch">
                  <label class="form-check-label" >Set Schedule</label>
               </div>
               <div class="monthlySetCourse" id="monthlySetCourse" style="display: none;">
                  <div class="row">
                     <div class="col-md-8 mt-4">
                        <label><b>Monthly Day Classes</b></label>
                        <select class="form-control" name="monthlyDayClasses" id="monthlyDayClasses">
                           <option value="">Select</option>
                           <option value="monday" {{ old('monthlyDayClasses') == 'monday' ? 'selected' : '' }}>Monday</option>
                           <option value="tuesday" {{ old('monthlyDayClasses') == 'tuesday' ? 'selected' : '' }}>Tuesday</option>
                           <option value="wednesday" {{ old('monthlyDayClasses') == 'wednesday' ? 'selected' : '' }}>Wednesday</option>
                           <option value="thursday" {{ old('monthlyDayClasses') == 'thursday' ? 'selected' : '' }}>Thursday</option>
                           <option value="friday" {{ old('monthlyDayClasses') == 'friday' ? 'selected' : '' }}>Friday</option>
                           <option value="saturday" {{ old('monthlyDayClasses') == 'saturday' ? 'selected' : '' }}>Saturday</option>
                           <option value="sunday" {{ old('monthlyDayClasses') == 'sunday' ? 'selected' : '' }}>Sunday</option>
                        </select>
                        @error('monthlyDayClasses')
                        <p class="alert alert-danger mt-1">{{$message}}</p>
                        @enderror
                     </div>
                     <div class="col-md-4">
                        <button type="button" class="btn btn-primary float-right mt-4" id="addMonthlySchedule"><i class="fas fa-plus-circle"></i> Add Monthly Schedule</button>
                     </div>
                     <div class="col-md-6 mt-4">
                        <label>Set Schedule Date</label>
                        <input type="date" name="scheduleCalender" id="scheduleCalender" class="form-control" value="{{ old('scheduleCalender') }}">
                        @error('scheduleCalender')
                        <p class="alert alert-danger mt-1">{{$message}}</p>
                        @enderror
                     </div>
                     <div class="col-md-3 mt-4">
                        <label>Set Schedule From</label>
                        <select class="form-control" name="monthlyDayTime">
                           <option value="">Select</option>
                           <option value="10am" {{ old('monthlyDayTime') == '10am' ? 'selected' : '' }}>10:00 AM</option>
                           <option value="11am" {{ old('monthlyDayTime') == '11am' ? 'selected' : '' }}>11:00 AM</option>
                           <option value="12pm" {{ old('monthlyDayTime') == '12pm' ? 'selected' : '' }}>12:00 PM</option>
                           <option value="1pm" {{ old('monthlyDayTime') == '1pm' ? 'selected' : '' }}>1:00 PM</option>
                           <option value="2pm" {{ old('monthlyDayTime') == '2pm' ? 'selected' : '' }}>2:00 PM</option>
                           <option value="3pm" {{ old('monthlyDayTime') == '3pm' ? 'selected' : '' }}>3:00 PM</option>
                           <option value="4pm" {{ old('monthlyDayTime') == '4pm' ? 'selected' : '' }}>4:00 PM</option>
                           <option value="5pm" {{ old('monthlyDayTime') == '5pm' ? 'selected' : '' }}>5:00 AM</option>
                           <option value="6pm" {{ old('monthlyDayTime') == '6pm' ? 'selected' : '' }}>6:00 AM</option>
                        </select>
                        @error('monthlyDayTime')
                        <p class="alert alert-danger mt-1">{{$message}}</p>
                        @enderror
                     </div>
                     <div class="col-md-3 mt-4">
                        <label>Set Schedule To</label>
                        <select class="form-control" name="monthlyDaysTime">
                           <option value="">Select</option>
                           <option value="11am" {{ old('monthlyDaysTime') == '11am' ? 'selected' : '' }}>11:00 AM</option>
                           <option value="12pm" {{ old('monthlyDaysTime') == '12pm' ? 'selected' : '' }}>12:00 PM</option>
                           <option value="1pm" {{ old('monthlyDaysTime') == '1pm' ? 'selected' : '' }}>1:00 PM</option>
                           <option value="2pm" {{ old('monthlyDaysTime') == '2pm' ? 'selected' : '' }}>2:00 PM</option>
                           <option value="3pm" {{ old('monthlyDaysTime') == '3pm' ? 'selected' : '' }}>3:00 PM</option>
                           <option value="4pm" {{ old('monthlyDaysTime') == '4pm' ? 'selected' : '' }}>4:00 PM</option>
                           <option value="5pm" {{ old('monthlyDaysTime') == '5pm' ? 'selected' : '' }}>5:00 AM</option>
                           <option value="6pm" {{ old('monthlyDaysTime') == '6pm' ? 'selected' : '' }}>6:00 AM</option>
                           <option value="7pm" {{ old('monthlyDaysTime') == '7pm' ? 'selected' : '' }}>7:00 PM</option>
                        </select>
                        @error('monthlyDaysTime')
                        <p class="alert alert-danger mt-1">{{$message}}</p>
                        @enderror
                     </div>
                     <br>
                  </div>
                  <div id="addMonthlyMoreSchedule">
                  </div>
               </div>
               <div class="row weeklySetCourse" style="display: none;" id="weeklySetCourse">
                  <div class="col-md-8 mt-4">
                     <label><b>Weekly Day Classes</b></label>
                     <select class="form-control" name="weeklyDayClasses">
                        <option value="">Select</option>
                        <option value="monday" {{ old('weeklyDayClasses') == 'monday' ? 'selected' : '' }}>Monday</option>
                        <option value="tuesday" {{ old('weeklyDayClasses') == 'tuesday' ? 'selected' : '' }}>Tuesday</option>
                        <option value="wednesday" {{ old('weeklyDayClasses') == 'wedesday' ? 'selected' : '' }}>Wednesday</option>
                        <option value="thursday" {{ old('weeklyDayClasses') == 'thursday' ? 'selected' : '' }}>Thursday</option>
                        <option value="friday" {{ old('weeklyDayClasses') == 'friday' ? 'selected' : '' }}>Friday</option>
                        <option value="saturday" {{ old('weeklyDayClasses') == 'saturday' ? 'selected' : '' }}>Saturday</option>
                        <option value="sunday" {{ old('weeklyDayClasses') == 'sunday' ? 'selected' : '' }}>Sunday</option>
                     </select>
                     @error('weeklyDayClasses')
                     <p class="alert alert-danger mt-1">{{$message}}</p>
                     @enderror
                  </div>
                  <div class="col-md-4">
                     <button type="button" class="btn btn-primary float-right mt-4" id="addWeeklySchedule"><i class="fas fa-plus-circle"></i> Add Weekly Schedule</button>
                  </div>
                  <div class="col-md-6 mt-4">
                     <label>Set Schedule Date</label>
                     <input type="date" name="scheduleCalender" class="form-control" value="{{ old('scheduleCalender') }}">
                     @error('scheduleCalender')
                     <p class="alert alert-danger mt-1">{{$message}}</p>
                     @enderror
                  </div>
                  <div class="col-md-3 mt-4">
                     <label>Set Schedule From</label>
                     <select class="form-control" name="weeklyDayTime">
                        <option value="">Select</option>
                        <option value="10am" {{ old('weeklyDayTime') == '10am' ? 'selected' : '' }}>10:00 AM</option>
                        <option value="11am" {{ old('weeklyDayTime') == '11am' ? 'selected' : '' }}>11:00 AM</option>
                        <option value="12pm" {{ old('weeklyDayTime') == '12pm' ? 'selected' : '' }}>12:00 PM</option>
                        <option value="1pm" {{ old('weeklyDayTime') == '1pm' ? 'selected' : '' }}>1:00 PM</option>
                        <option value="2pm" {{ old('weeklyDayTime') == '2pm' ? 'selected' : '' }}>2:00 PM</option>
                        <option value="3pm" {{ old('weeklyDayTime') == '3pm' ? 'selected' : '' }}>3:00 PM</option>
                        <option value="4pm" {{ old('weeklyDayTime') == '4pm' ? 'selected' : '' }}>4:00 PM</option>
                        <option value="5pm" {{ old('weeklyDayTime') == '5pm' ? 'selected' : '' }}>5:00 AM</option>
                        <option value="6pm" {{ old('weeklyDayTime') == '6pm' ? 'selected' : '' }}>6:00 AM</option>
                     </select>
                     @error('weeklyDayTime')
                     <p class="alert alert-danger mt-1">{{$message}}</p>
                     @enderror
                  </div>
                  <div class="col-md-3 mt-4">
                     <label>Set Schedule To</label>
                     <select class="form-control" name="weeklyDaysTime">
                        <option value="">Select</option>
                        <option value="11am" {{ old('weeklyDaysTime') == '11am' ? 'selected' : '' }}>11:00 AM</option>
                        <option value="12pm" {{ old('weeklyDaysTime') == '12pm' ? 'selected' : '' }}>12:00 PM</option>
                        <option value="1pm" {{ old('weeklyDaysTime') == '1pm' ? 'selected' : '' }}>1:00 PM</option>
                        <option value="2pm" {{ old('weeklyDaysTime') == '2pm' ? 'selected' : '' }}>2:00 PM</option>
                        <option value="3pm" {{ old('weeklyDaysTime') == '3pm' ? 'selected' : '' }}>3:00 PM</option>
                        <option value="4pm" {{ old('weeklyDaysTime') == '4pm' ? 'selected' : '' }}>4:00 PM</option>
                        <option value="5pm" {{ old('weeklyDaysTime') == '5pm' ? 'selected' : '' }}>5:00 AM</option>
                        <option value="6pm" {{ old('weeklyDaysTime') == '6pm' ? 'selected' : '' }}>6:00 AM</option>
                        <option value="7pm" {{ old('weeklyDaysTime') == '7pm' ? 'selected' : '' }}>7:00 PM</option>
                     </select>
                     @error('weeklyDaysTime')
                     <p class="alert alert-danger mt-1">{{$message}}</p>
                     @enderror
                  </div>
                  <br>
               </div>
               <div id="addWeeeklyMoreSchedule">
               </div>
               <div class="my-3"></div>
               <!-- Material checked -->
               <div class="form-check"  style="font-size: 15px;">
                  <input type="radio" class="form-check-input free-sechdule" id="freesch" name="setsch" value="freesch">
                  <label class="form-check-label">Free Schedule</label>
               </div>
               <textarea style="display:none;" rows="5" cols="70" type="text" class="form-control otherAnswer" name="otherAnswer" id="otherAnswer" ></textarea>
               @error('otherAnswer')
               <p class="alert alert-danger mt-1">{{$message}}</p>
               @enderror
               <div id="addCourseSch">
                    <hr>
                    <hr>
               </div>
            </div>
         </div>
       
         <div class="text-center mt-6"><br>
            <input type="submit" name="" value="Add Course"   class="btn btn-outline-success">
         </div>
      </form>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

@endsection