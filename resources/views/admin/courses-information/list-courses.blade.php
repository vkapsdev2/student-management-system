@section('title','View All List')
@extends('layout.default')
@section('content')
	<div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Course Details
                </h3>
            </div>
            <div class="card-toolbar">
                <a href="{{route('add-course-information')}}" class="btn btn-primary font-weight-bolder">
                <span class="svg-icon svg-icon-md">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <circle fill="#000000" cx="9" cy="15" r="6"/>
                            <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>New Course</a>
                <!--end::Button-->
            </div>
        </div>

        <div class="card-body">

            <!--begin::Search Form-->
            <div class="mt-2 mb-5 mt-lg-5 mb-lg-10">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Search..." id=""/>
                                    <span><i class="flaticon2-search-1 text-muted"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->
            @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
        <div class="delete_message">
        </div>
            <table class="table table-bordered table-hover" id="kt_datatable">
                <thead>
                <tr class="text-center">
                    <th>#</th>
                    <th>Course Name</th>
                    <th>Staff Incharge</th>
                    <th>Supporting Staff</th>
                    <th>Types Of Classes</th>
                    <th>Number Of Classes</th>
                     <th>Number Of Hours</th>
                    <th>Total Hours</th>
                    <th>Action</th>

                </tr>
                </thead>
                <tbody id="searchDataTable">
                    @forelse($coursesImformationData as $studentKey => $courseValue)

                        <tr class="text-center">
                           	<td>{{$studentKey+1}}</td>
                            <td>

                                {{ $courseValue->courseName }}
                            </td>
                            <td>{{$courseValue->staffIncharge}}</td>
                            <td>{{$courseValue->supportStaff}}</td>
                            <td>{{$courseValue->typeOfClasses}}</td>
                            <td>{{$courseValue->numOfClasses}}</td>
                            <td>{{$courseValue->numOfHours}}</td>
                            <td>{{$courseValue->totalnumOfHours}}</td>
                            <td width="220">
                                <a href="#" class="btn btn-secondary btn-circle mx-2" data-toggle="tooltip" data-original-title="Edit"><i class="fas fa-pencil-alt "></i>
                                </a>
                                <a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle mx-2" onclick="comfrimDelete('{{$courseValue->id}}','{{$courseValue->courseName}}')"><i class="fas fa-times"></i>
                                </a>
                                <a href="{{url('single-course-details/'.$courseValue->id)}}" class="btn btn-primary btn-circle mx-2" data-toggle="tooltip" data-original-title="View All Detail"><i class="fas fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                    @empty
                    <td class="text-center text-danger" colspan="6">No Data Found</td>
                    @endforelse
                </tbody>
            </table>
            <div class="text-center">

            </div>
            <style type="text/css">
                .w-5{
                    display:inline;
                    width: 20px;
                }
                .flex-1{
                    display:none;
                }
            </style>
        </div>

    </div>
    <script type="">
    	function comfrimDelete(id,courseName) {
            Swal.fire({
                title: 'Are you sure want to delete '+courseName+'?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax("{{url('delete-course')}}/"+id,
                    {
                        method: 'GET',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    }).done(function (r) {
                        console.log('result',r);

                        if(r.success){
                            $("div.delete_message").html('<div class="alert alert-success">' + r.message + '</div>');
                            window.setTimeout(function(){location.reload()},1000);
                        }else{
                            swal.fire({
                                text: 'Course does not exist..',
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn font-weight-bold btn-light-primary"
                                }
                            }).then(function () {
                                KTUtil.scrollTop();
                            });
                        }
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        swal.fire({
                            text: errors,
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn font-weight-bold btn-light-primary"
                            }
                        }).then(function () {
                            KTUtil.scrollTop();
                        });
                    });
                }
            });
        }
    </script>
@endsection