@section('title','Add Course')
@extends('layout.default')
@section('content')
<div class="container-fluid">
    <div class="col-md-10 mx-auto">
        <h2 class="text-center">Add Course Details</h2>
        <hr>
        @if(session('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>{{session('success')}}</strong>
        </div>
        @endif
        @if(session('dataMatchingError'))
        <div class="alert alert-danger">
          {{session('dataMatchingError')}}
        </div>
        @endif
        <form action="{{ route('save-course') }}" method="post">
            <div class="card-body">
            	@csrf
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-10 mt-2 mx-auto">
                        <label>Course Name</label>
                        <input type="text" name="courseName" placeholder="Enter course name" class="form-control">
                        @error('courseName')
                            <p class="alert alert-danger mt-1">{{$message}}</p>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-10 mt-2 mx-auto">
                             <label>Total Course Duration</label>
                            <select class="form-control" name="typeOfClasses" id="typeOfClasses">
                                <option value="" >Select</option>
                                <option value="crashCourse" {{ old('typeOfClasses') == 'crashCourse' ? 'selected' : '' }}>Crash Course</option>
                                <option value="1monthCourse" {{ old('typeOfClasses') == '1monthCourse' ? 'selected' : '' }}>1 Month Course</option>
                                <option value="2monthCourse" {{ old('typeOfClasses') == '2monthCourse' ? 'selected' : '' }}>2 Month Course</option>
                                <option value="3monthCourse" {{ old('typeOfClasses') == '3monthCourse' ? 'selected' : '' }}>3 Month Course</option>
                                <option value="6monthCourse" {{ old('typeOfClasses') == '6monthCourse' ? 'selected' : '' }}>6 Month Course</option>
                                <option value="1yearCourse" {{ old('typeOfClasses') == '1yearCourse' ? 'selected' : '' }}>1 Yearly Course</option>
                            </select>
                            @error('typeOfClasses')
                                <p class="alert alert-danger mt-1">{{$message}}</p>
                            @enderror
                        </div> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-10 mt-2 mx-auto">
                                <label>Course Description</label>
                                <textarea name="courseDetails" placeholder="Enter course details" class="form-control"></textarea>
                                @error('courseDetails')
                                    <p class="alert alert-danger mt-1">{{$message}}</p>
                                @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                        <div class="row">
                            <div class="col-md-10 mt-2 mx-auto">
                                <input type="submit" name="create-course" value="Add Course" class="btn btn-sm btn-success">
                            </div>
                        </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

