<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Student\StudentController;
use App\Http\Controllers\Admin\Student\Academic\AcademicController;
use App\Http\Controllers\Admin\Student\Goals\GoalsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\Staff\StaffController;
use App\Http\Controllers\Admin\CourseImformation\CourseImformation;
use App\Http\Controllers\Admin\Course\CourseController;
// use App\Http\Controllers\Admin\Student\StudentLoginController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/command', function () {
    	\Artisan::call('config:clear');
    	\Artisan::call('config:cache');
    	\Artisan::call('cache:clear');

    dd('cache clear successfully');
});

Route::group(['middleware' => ['auth']], function(){
	/*Route::get('/', function () {
    	return view('pages.dashboard');
	});*/

	// -------- Admin Adding Course details ----------------------------

	Route::get('/add-course-information',[CourseImformation::class,'create'])->name('add-course-information');
	Route::get('/list-course-information',[CourseImformation::class,'index'])->name('list-course-imformation');
	Route::post('/save-course-information',[CourseImformation::class,'store'])->name('save-course-information');
	Route::get('/single-course-details/{id}',[CourseImformation::class,'show'])->name('single-course-details');
	// -----------------------------------------------------------------

	Route::get('/', [HomeController::class, 'index'])->name('home1');
	Route::get('/dashboard', [HomeController::class, 'index'])->name('home');

	Route::get('/my-profile',[AdminController::class,'edit'])->name('my-profile');
	// route forn student details and signup
	Route::get('/student',[StudentController::class,'index'])->name('view-all-student');

	Route::get('/create-student-profile',[StudentController::class,'create'])->name('create-student-profile');

	Route::post('/save-student',[StudentController::class,'store'])->name('save-student');

	Route::get('/edit/{id}',[StudentController::class,'edit'])->name('edit/{$id}');

	Route::post('/update-student',[StudentController::class,'update'])->name('update-student');

	Route::get('/show-student/{id}',[StudentController::class,'show'])->name('show-student');

	// Route::get('delete-student/{id}',[StudentController::class,'destroy'])->name('delete-student');
	Route::get('/delete-student/{id}',[StudentController::class,'destroy'])->name('delete-student');

	Route::get('/search-student-record',[StudentController::class,'searchStudentRecord'])->name('search-student-record');
	Route::get('/student-academic',[AcademicController::class,'index'])->name('student-academic');

	Route::post('/save-academic-student-details',[AcademicController::class,'store'])->name('save-academic-student-details');
	Route::get('/show-student-academic-details/{id}',[AcademicController::class,'edit'])->name('show-student-academic-details');
	Route::post('/update-academic-student-details',[AcademicController::class,'update'])->name('update-academic-student-details');
	/*Staff routing*/
	Route::get('staff-list',[StaffController::class,'index'])->name('view-all-staff');

	Route::get('add-staff',[StaffController::class,'create'])->name('create-staff');
	Route::post('save-staff',[StaffController::class,'store'])->name('save-staff');

	Route::get('view-staff-profile/{id}',[StaffController::class,'show'])->name("view-staff-profile");

	Route::get('edit-staff-profile/{id}',[StaffController::class,'edit'])->name('edit-staff-profile');
	Route::post('update-staff',[StaffController::class,'update'])->name('update-staff-profile');

	/*COurse list*/

	ROute::get('course-list',[CourseController::class,'index'])->name('view-course-list');

	Route::get('add-course',[CourseController::class,'create'])->name('add-course');
	Route::post('save-course',[CourseController::class,'store'])->name('save-course');

//=============================== Student Goals ==============================================
	Route::get('create-goals',[GoalsController::class,'index'])->name('create-goals');
});



// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


// Route::get('/home', 'PagesController@index');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



// Demo routes
Route::get('/datatables', 'PagesController@datatables');
Route::get('/ktdatatables', 'PagesController@ktDatatables');
Route::get('/select2', 'PagesController@select2');
Route::get('/jquerymask', 'PagesController@jQueryMask');
Route::get('/icons/custom-icons', 'PagesController@customIcons');
Route::get('/icons/flaticon', 'PagesController@flaticon');
Route::get('/icons/fontawesome', 'PagesController@fontawesome');
Route::get('/icons/lineawesome', 'PagesController@lineawesome');
Route::get('/icons/socicons', 'PagesController@socicons');
Route::get('/icons/svg', 'PagesController@svg');

// Quick search dummy route to display html elements in search dropdown (header search)
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');



// ................................login Route.....................................................

// Route::post('login', 'Auth\LoginController@authenticate');

 // Route::get('/datatables/logout', [StudentLoginController::class,'logout'])->name('logout');

// ...............................Academic Routs....................................................


