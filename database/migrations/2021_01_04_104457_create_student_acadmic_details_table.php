<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentAcadmicDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_acadmic_details', function (Blueprint $table) {
            $table->id();
            $table->string("studentId");
            $table->string("schoolName");
            $table->string("standard");
            $table->string("class");
            $table->string("semesterName");
            $table->string("semesterGrade");
            $table->string("semesterFile");
            $table->string("otherInformationName");
            $table->string("otherInformationFile");
            $table->string("schoolAchievementName");
            $table->string("schoolAchievementFile");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_acadmic_details');
    }
}
