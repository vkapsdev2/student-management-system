<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('japaneseName')->nullable();
            $table->integer('age');
            $table->date('dob')->nullable();
            $table->text('address')->nullable();
            $table->string('faxNumber')->nullable();
            $table->string('homeNumber')->nullable();
            $table->string('mobileNumber')->nullable();
            $table->string('email');
            $table->string('password');
            $table->enum('gender',['Male','Female','Other'])->nullable();
            $table->string('guardianName')->nullable();
            $table->text('guardianAddress')->nullable();;
            $table->integer('guardianRelationship')->nullable();
            $table->string('guardianPhoneNumber')->nullable();
            $table->string('guardianFaxNumber')->nullable();
            $table->enum('guardianGender',['Male','Female','Other'])->nullable();
            $table->string('guardianOccupation')->nullable();
            $table->string('companyName')->nullable();
            $table->text('companyAddress')->nullable();
            $table->string('companyPhoneNumber')->nullable();
            $table->string('emergencyName')->nullable();
            $table->text('emergencyAddress')->nullable();
            $table->string('emergencyEmail')->nullable();
            $table->string('emergencyCellphoneNumber')->nullable();
            $table->string('emergencyHomeNumber')->nullable();
            $table->string('emergencyFaxNumber')->nullable();
            $table->enum('emergencyGender',['Male','Female','Other'])->nullable();
            $table->integer('emergencyRelationshipStudent')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
