<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseImformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_imformations', function (Blueprint $table) {
            $table->id();
            $table->string("courseName");
            $table->bigInteger("student_id");
            $table->string("staffIncharge");
            $table->string("supportStaff");
            $table->string("typeOfClasses");
            $table->bigInteger("numOfClasses");
            $table->string("numOfHours");
            $table->string("totalnumOfHours");
            $table->string("weeklyDayClasses")->nullable();
            $table->date("scheduleCalender")->nullable();
            $table->dateTime("weeklyDayTime")->nullable();
            $table->dateTime("weeklyDaysTime")->nullable();
            $table->string("otherAnswer")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_imformations');
    }
}
