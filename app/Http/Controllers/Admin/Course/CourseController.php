<?php

namespace App\Http\Controllers\Admin\Course;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Course;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courseData = Course::all();
        return view("admin.courses.list-courses",compact('courseData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.courses.add-courses");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'courseName'       =>  'required|string|max:25|unique:courses,course_name',
            'typeOfClasses'    =>  'required',
            'courseDetails'     =>  ['required', 'string', 'max:255',],
        ],
        [
            'courseName.required'    => 'Name is required.',
            'typeOfClasses'         => 'Select Course Duration',
            'courseDetails.required' => "Course Details is required",
            'courseDetails.unique'   => 'courseDetails already exists.',
        ])->validate();
        $data = Course::create([
            'course_name'           =>  $request->input('courseName'),
            'course_duration'       =>  $request->input('typeOfClasses'),
            'course_description'         =>  $request->input('courseDetails'),
        ]);
        if($data){
            $request->session()->flash('success', 'Project Manager added successfully .');
            return redirect()->route('view-course-list');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
