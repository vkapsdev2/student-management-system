<?php

namespace App\Http\Controllers\Admin\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\StudentAcadmicDetail;
use Illuminate\Support\Facades\Hash;
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        $studentData =  User::whereHas('roles', function($role) {
            $role->where('roles','=','Student');
        })->paginate(5);  
        return view('admin.students.view-all-students',compact('studentData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){
        return view('admin.students.create-student');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){
        $request->validate([
            'name'=>'required',
            'age'=>'numeric|min:1',
            'gender'=>'required',
            'dob'=>'required',
            'faxNumber'=>'required|numeric',
            'homeNumber'=>'required|numeric',
            'mobileNumber'=>'required|numeric',
            'email'=>'required|email|unique:users',
            'address'=>'required',
            // 'password'=>'required|min:4',
            // 'confirmPassword'=>'required|same:password|min:4',
            'emergencyName'=>'required|different:name',
            'emergencyEmail'=>'required|different:email',
            'emergencyGender'=>'required',
            'emergencyRelationshipStudent'=>'required',
            'emergencyCellNumber'=>'required|different:mobileNumber|numeric',
            'emergencyHomeNumber'=>'required|different:homeNumber|numeric',
            'emergencyFaxNumber'=>'required|different:faxNumber|numeric',
            'emergencyAddress'=>'required'

        ],[
            'name.required'=>'Name required',
            'gender.required'=>'Gender required',
            'dob.required'=>'Date Of Birth required',
            'age.required'=>'Required',
            'faxNumber.required'=>'Fax Number required',
            'homeNumber.required'=>'Home Number required',
            'mobileNumber.required'=>'Mobile Number required',
            'email.required'=>'Email required',
            'email.email'=>'Email not valid',
            'address.required'=>'Address required',
            'emergencyName.required'=>'Name required',
            'emergencyEmail.required'=>'Email required',
            'emergencyGender.required'=>'required',
            'emergencyRelationshipStudent.required'=>'required',
            'emergencyCellNumber.required'=>'Mobile number required',
            'emergencyHomeNumber.required'=>'Home number required',
            'emergencyFaxNumber.required'=>'Fax number required',
            'emergencyAddress.required'=>'Address required',

        ]);
        $password=Hash::make('12345');

        $user=User::create([
            'name'=>$request->name,
            'japaneseName'=>$request->japaneseName,
            'age'=>$request->age,
            'gender'=>$request->gender,
            'dob'=>$request->dob,
            'faxNumber'=>$request->faxNumber,
            'homeNumber'=>$request->homeNumber,
            'mobileNumber'=>$request->mobileNumber,
            'email'=>$request->email,
            'address'=>$request->address,
            'password'=>$password,
            'guardianName'=>$request->guardianName,
            'guardianAddress'=>$request->guardianAddress,
            'guardianRelationship'=>$request->guardianRelationship,
            'guardianPhoneNumber'=>$request->guardianPhoneNumber,
            'guardianFaxNumber'=>$request->guardianFaxNumber,
            'guardianGender'=>$request->guardianGender,
            'guardianOccupation'=>$request->guardianOccupation,
            'companyName'=>$request->companyName,
            'companyAddress'=>$request->companyAddress,
            'companyPhoneNumber'=>$request->companyNumber,
            'emergencyName'=>$request->emergencyName,
            'emergencyEmail'=>$request->emergencyEmail,
            'emergencyAddress'=>$request->emergencyAddress,
            'emergencyHomeNumber'=>$request->emergencyHomeNumber ,
            'emergencyCellphoneNumber'=>$request->emergencyCellNumber,
            'emergencyFaxNumber'=>$request->emergencyFaxNumber,
            'emergencyGender'=>$request->emergencyGender,
            'emergencyRelationshipStudent'=>$request->emergencyRelationshipStudent,
        ]);

        $user->save();
        $user->addRole(Role::Student);
        if($user){
            return redirect('create-student-profile')->with('success','Data Insert Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id){
        $studentData=User::findOrFail($id);
        $studentAcademicData=StudentAcadmicDetail::where('studentId',$id)->first();
        if($studentData)
        return view('admin.students.student-details',compact("studentData","studentAcademicData"));
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id){
        $studentData=User::findOrFail($id);
        if($studentData)
        return view('admin.students.edit-student',compact("studentData"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
      // dd($request->all());
        $update=User::where('id','=',$request->studentId)
        ->update([
          'name'=>$request->name,
          'japaneseName'=>$request->japaneseName,
          'age'=>$request->age,
          'gender'=>$request->gender,
          'dob'=>$request->dob,
          'faxNumber'=>$request->faxNumber,
          'homeNumber'=>$request->homeNumber,
          'mobileNumber'=>$request->mobileNumber,
          'email'=>$request->email,
          'address'=>$request->address,
          'guardianName'=>$request->guardianName,
          'guardianAddress'=>$request->guardianAddress,
          'guardianRelationship'=>$request->guardianRelationship,
          'guardianPhoneNumber'=>$request->guardianPhoneNumber,
          'guardianFaxNumber'=>$request->guardianFaxNumber,
          'guardianGender'=>$request->guardianGender,
          'guardianOccupation'=>$request->guardianOccupation,
          'companyName'=>$request->companyName,
          'companyAddress'=>$request->companyAddress,
          'companyPhoneNumber'=>$request->companyNumber,
          'emergencyName'=>$request->emergencyName,
          'emergencyEmail'=>$request->emergencyEmail,
          'emergencyAddress'=>$request->emergencyAddress,
          'emergencyHomeNumber'=>$request->emergencyHomeNumber ,
          'emergencyCellphoneNumber'=>$request->emergencyCellphoneNumber,
          'emergencyFaxNumber'=>$request->emergencyFaxNumber,
          'emergencyGender'=>$request->emergencyGender,
          'emergencyRelationshipStudent'=>$request->emergencyRelationshipStudent,
        ]);
        if($update)
        {
          return redirect('student')->with('success','Student Data Update Successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id){ 
        $delete = User::findOrFail($id)->delete($id);
        $RoleUser = RoleUser::where('user_id',$id)->delete($id);
        if($delete){
            echo 'success';
        }
    }

    /* ===== search student record in view all papes ===== */
    public function searchStudentRecord(Request $request){
        $searchval=$request->searchval;
        $searchData = User::wherehas('roles',function($role){

        })->where('name', 'LIKE', '%'.$searchval.'%')->orWhere('email', 'LIKE', '%'.$searchval.'%')->get();
        
        if($searchData->toArray()){
            $res = ['status' => true,'success' => $searchData];
        }else{
            $res = ['status' => false,'success' => $searchData];
        }
        echo json_encode($res);
    }
}
