<?php

namespace App\Http\Controllers\Admin\Student\Academic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Models\StudentAcadmicDetail;
use Illuminate\Support\Facades\DB;
use File;

class AcademicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
 public function index(Request $request)
    {
        // dd($request->get("id"));
        if($request->get("id"))
        {
            $urlId =  User::whereHas('roles', function($role) {
                $role->where('roles','=','Student');
            })->where('id', $request->get("id"))->get(['id','name']);
        }
        else
        {
            $urlId=0;
        }
        $StudentAcadmicDetail = StudentAcadmicDetail::get();
        $ids = [];
        for ($i=0; $i < count($StudentAcadmicDetail) ; $i++) { 
            $ids[] = $StudentAcadmicDetail[$i]['studentId'] ;
        }
       
        $studentData =  User::whereHas('roles', function($role) {
            $role->where('roles','=','Student');
        })->whereNotIn('id', $ids)->get(['id','name']);
        return view('admin.students.academic.academicInformation',compact('studentData','urlId'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'schoolName'=>'required',
            'studentStandard'=>'required',
            'studentClass'=>'required',
            'studentSemesterName'=>'required',
            'studentSemesterGrade'=>'required'
        ],[
            'schoolName.required'=>'Please enter school name',
            'studentStandard.required'=>'Please enter student standard',
            'studentClass.required'=>'Please enter student class',
            'studentSemesterName.required'=>'Please enter student semester'
        ]);
        if($request->hasfile('studentSemesterFile'))
        {
            foreach($request->file('studentSemesterFile') as $image) 
            {
                $fileName=rand().$image->getClientOriginalName();
                $image->move(public_path().'/academicStudentImages/semesterImages',$fileName);
                $semesterFile[]=$fileName;
            }
        }
        else{
            $semesterFile=null;
        }



         if($request->hasfile('otherInformatonFile'))
        {
            foreach($request->file('otherInformatonFile') as $image) 
            {
                $fileName=rand().$image->getClientOriginalName();
                $image->move(public_path().'/academicStudentImages/otherInformationImages',$fileName);
                $otherInformatonFile[]=$fileName;
            }
        }
        else{
            $otherInformatonFile=null;
        }

         if($request->hasfile('schoolAchievementFile'))
        {
            foreach($request->file('schoolAchievementFile') as $image) 
            {
                $fileName=rand().$image->getClientOriginalName();
                $image->move(public_path().'/academicStudentImages/schoolAchievementImages',$fileName);
                $schoolAchievementFile[]=$fileName;
            }
        }
        else{
            $schoolAchievementFile=null;
        }

        $store=StudentAcadmicDetail::create([
            'studentId'=>$request->studentId,
            'schoolName'=>$request->schoolName,
            'standard'=>$request->studentStandard,
            'class'=>$request->studentClass,
            'semesterName'=>json_encode($request->studentSemesterName),
            'semesterGrade'=>json_encode($request->studentSemesterGrade),
            'semesterFile'=>json_encode($semesterFile),
            'otherInformationName'=>json_encode($request->otherInformationName),
            'otherInformationFile'=>json_encode($otherInformatonFile),
            'schoolAchievementName'=>json_encode($request->schoolAchievementName),
            'schoolAchievementFile'=>json_encode($schoolAchievementFile)
        ]);
        if($store){
            return redirect('student-academic')->with('success','Academic details upload successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $studentData=User::find($id);
        $academicData=StudentAcadmicDetail::where('studentId',$id)->first();
        // $academicData=StudentAcadmicDetail::where('studentId',$id)->get();
        // dd(json_decode($academicData->semesterName));
        $academicData['semesterName']=json_decode($academicData->semesterName);
        $academicData['semesterGrade']=json_decode($academicData->semesterGrade);
        $academicData['semesterFile']=json_decode($academicData->semesterFile);
        $academicData['otherInformationName']=json_decode($academicData->otherInformationName);
        $academicData['otherInformationFile']=json_decode($academicData->otherInformationFile);
        $academicData['schoolAchievementName']=json_decode($academicData->schoolAchievementName);
        $academicData['schoolAchievementFile']=json_decode($academicData->schoolAchievementFile);
        if($academicData['semesterName'])
            $semesterCount=count($academicData['semesterName']);
        else
            $semesterCount=0;
        if($academicData['otherInformationName'])
        $otherCount=count($academicData['otherInformationName']);
        else
            $otherCount=0;
        if($academicData['schoolAchievementName'])
        $schoolCount=count($academicData['schoolAchievementName']);
        else
            $schoolCount=0;
        return view('admin.students.academic.update-student-academic-details',compact('studentData','academicData','semesterCount','otherCount','schoolCount'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $id=$request->studentId;
        $academicData=StudentAcadmicDetail::where('studentId',$id)->first();
        $oldSemesterFile=json_decode($academicData->semesterFile);
        $oldOtherFile=json_decode($academicData->otherInformationFile);
        $oldSchoolFile=json_decode($academicData->schoolAchievementFile);

        if($request->hasfile('studentSemesterFile'))
        {
            foreach($request->file('studentSemesterFile') as $image) 
            {
                $image_path=public_path().'/academicStudentImages/semesterImages/'.$image->getClientOriginalName();
                if(File::exists($image_path))
                    File::delete($image_path);
                $fileName=rand().$image->getClientOriginalName();
                $image->move(public_path().'/academicStudentImages/semesterImages/',$fileName);
                $semesterFile[] = $fileName;
            }
            if($oldSemesterFile)
                $semesterFile = array_merge($oldSemesterFile, $semesterFile);
        }
        else{
            $semesterFile=json_decode($academicData->semesterFile);
        }
        if($request->hasfile('otherInformatonFile'))
        {
            foreach($request->file('otherInformatonFile') as $image) 
            {
                $image_path=public_path().'/academicStudentImages/otherInformationImages/'.$image->getClientOriginalName();
                if(File::exists($image_path))
                    File::delete($image_path);
                $fileName=rand().$image->getClientOriginalName();
                $image->move(public_path().'/academicStudentImages/otherInformationImages',$fileName);
                $otherInformatonFile[]=$fileName;
            }
            if($oldOtherFile)
                $otherInformatonFile = array_merge($oldOtherFile, $otherInformatonFile);

        }
        else{
            $otherInformatonFile=json_decode($academicData->otherInformationFile);
        }

        if($request->hasfile('schoolAchievementFile'))
        {
            foreach($request->file('schoolAchievementFile') as $image) 
            {
                $image_path=public_path().'/academicStudentImages/schoolAchievementImages/'.$image->getClientOriginalName();
                if(File::exists($image_path))
                    File::delete($image_path);
                $fileName=rand().$image->getClientOriginalName();
                $image->move(public_path().'/academicStudentImages/schoolAchievementImages',$fileName);
                $schoolAchievementFile[]=$fileName;
            }
            if($oldSchoolFile)
            $schoolAchievementFile=array_merge($oldSchoolFile, $schoolAchievementFile);
        }
        else{
            $schoolAchievementFile=json_decode($academicData->schoolAchievementFile);
        }
        $update=StudentAcadmicDetail::where('studentId',$id)
        ->update([
            'schoolName'=>$request->schoolName,
            'standard'=>$request->studentStandard,
            'class'=>$request->studentClass,
            'semesterName'=>json_encode($request->studentSemesterName),
            'semesterGrade'=>json_encode($request->studentSemesterGrade),
            'semesterFile'=>json_encode($semesterFile),
            'otherInformationName'=>json_encode($request->otherInformationName),
            'otherInformationFile'=>json_encode($otherInformatonFile),
            'schoolAchievementName'=>json_encode($request->schoolAchievementName),
            'schoolAchievementFile'=>json_encode($schoolAchievementFile)
        ]);
        if($update)
            return redirect()->route('show-student-academic-details',['id'=>$id])->with('success',"Data update successfully");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
