<?php

namespace App\Http\Controllers\Admin\CourseImformation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CourseImformation as CourseImformationData;
use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Course;
use Illuminate\Support\Facades\Hash;

class CourseImformation extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coursesImformationData = CourseImformationData::orderBy('id', 'DESC')->get();
        
        /*$course_data = [];
        foreach ($coursesImformationData as $coursesImformationDatakey => $coursesImformationDataValue) {*/
            $course_data = Course::all()->get('course_id','course_name','course_duration');
        return view("admin.courses-information.list-courses",compact('coursesImformationData','course_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $studentData =  User::whereHas('roles', function($role) {
            $role->where('roles','=','Student');
        })->get(['id','name']);
        $staffData =  User::whereHas('roles', function($role) {
            $role->where('roles','=','Staff');
        })->get(['id','name']);

        $coursesData = Course::all();
          return view("admin.courses-information.add-courses",compact('coursesData','studentData','staffData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         // dd($request->all());
       $request->validate([
            'student'=>'required',
            'courseName'=>'required',
            'staffIncharge'=>'required',
            'supportStaff'=>'required',
            'typeOfClasses'=>'required',
            'numOfClasses'=>'required|numeric',
            'numOfHours'=>'required|numeric',
            //'totalnumOfHours'=>'required|numeric',
            //'weeklyDayClasses'=>'required',
            //'scheduleCalender'=>'required',
            //'weeklyDayTime'=>'required',
            //'weeklyDaysTime'=>'required',
            // 'otherAnswer'=>'required',

        ],[
            'student.required'=>'Select Student Name',
            'courseName.required'=>'Course Name required',
            'staffIncharge.required'=>'Staff Incharge required',
            'supportStaff.required'=>'Supporting Staff required',
            'typeOfClasses.required'=>'Required',
            'numOfClasses.required'=>'Fill Number of classes required',
            'numOfHours.required'=>'Fill Number of Hours required',
            /*'totalnumOfHours.required'=>'Total Number of Hours required',
            'weeklyDayClasses.required'=>'weekly Day Classes required',
            'scheduleCalender.email'=>'schedule Calender required',
            'weeklyDayTime.required'=>'Starting Time required',
            'weeklyDaysTime.required'=>'Ending Time required',*/
            // 'otherAnswer.required'=>'Other schedule required',

        ]);

            $courseInfo=CourseImformationData::create([
            'student_id'=>$request->student,
            'courseName'=>$request->courseName,
            'staffIncharge'=>$request->staffIncharge,
            'supportStaff'=>$request->supportStaff,
            'typeOfClasses'=>$request->typeOfClasses,
            'numOfClasses'=>$request->numOfClasses,
            'numOfHours'=>$request->numOfHours,
            'totalnumOfHours'=>$request->totalnumOfHours,
            'weeklyDayClasses'=>$request->weeklyDayClasses,
            'scheduleCalender'=>$request->scheduleCalender,
            'weeklyDayTime'=>$request->weeklyDayTime,
            'weeklyDaysTime'=>$request->weeklyDaysTime,
            'otherAnswer'=>$request->otherAnswer,
        ]);
        if($courseInfo){
            // return 'hello';
             return redirect('add-course-information')->with('success','Data Insert Successfully');
          }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $coursesData=CourseImformationData::find($id);

        if(!$coursesData->student_id == 0){

            $check_student_data = User::where("id",$coursesData->student_id)->exists();

            if($check_student_data){
                $student_data = User::find($coursesData->student_id)->get("name");
            }else{
                $student_data = "";
            }
        }else{
            $student_data = "";
        }

        return view('admin.courses-information.singleCourseDetails',compact("coursesData","student_data"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete=CourseImformationData::where('id','=',$id)->delete();
         if($delete){
          return response()->json(['success' => true,'message'=> 'Course has been deleted']);
        }
    }
}
