<?php

namespace App\Http\Controllers\Admin\Staff;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Validator;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staffData =  User::whereHas('roles', function($role) {
            $role->where('roles','=','Staff');
        })->paginate(10);
      // dd($studentData);
      return view('admin.staff.view-staff-list',compact('staffData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.staff.create-staff');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        Validator::make($request->all(), [
            'full_name'     =>  ['required', 'string', 'max:255',],
            'email'         =>  ['required', 'string', 'email', 'max:255'],
            'contact'       =>  ['required', 'numeric'],
            'gender'        =>  ['required'],
            'address'       =>  ['required','string'],
        ],
        [
            'full_name.required'    => 'Name is required.',
            'email.required'        => "Email is required",
            'email.unique'          => 'Email already exists.',
            'gender.required'      => 'Gender is required.',
            'contact.required'      => 'Contact is required.',
            'address.required'      => 'Address is required.',
        ])->validate();
        $password=Hash::make('staff12345');
        $user = User::create([
                'name'          =>  $request->input('full_name'),
                'email'         =>  $request->input('email'),
                'password'      =>  $password,
                'gender'        =>  $request->input('gender'),
                'mobileNumber'  =>  $request->input('contact'),
                'address'       =>  $request->input('address'),
            ]);
            $user->save();
            $user->addRole(Role::Staff);
            if($user){
                $request->session()->flash('success', 'Staff` added successfully .');
            }
            return redirect()->route('view-all-staff');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $studentData = User::find($id);

        return view('admin.staff.staff-profile',compact('studentData'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staff_data = User::find($id);

        return view('admin.staff.edit-staff',compact('staff_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $staff_id = $request->input("staff_id");
        // dd($request->all());
        Validator::make($request->all(), [
            'name'          =>  ['required', 'string', 'max:255',],
            'email'         =>  ['required', 'string', 'email', 'max:255'],
            'mobileNumber'  =>  ['required', 'numeric'],
            'gender'        =>  ['required'],
            'address'       =>  ['required','string'],
        ],
        [
            'name.required'         => 'Name is required.',
            'email.required'        => "Email is required",
            'email.unique'          => 'Email already exists.',
            'gender.required'       => 'Gender is required.',
            'mobileNumber.required'      => 'Contact is required.',
            'address.required'      => 'Address is required.',
        ])->validate();
        // dd($request->all());
        $data = User::find($staff_id)->update([
            'name'          =>  $request->input('name'),
            'email'         =>  $request->input('email'),
            'gender'        =>  $request->input('gender'),
            'mobileNumber'  =>  $request->input('mobileNumber'),
            'address'       =>  $request->input('address'),
        ]);

        if($data){
            $request->session()->flash('success', 'Staff Profile successfully updated.');
            return redirect()->route('view-all-staff');  
        }  
        /**/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
