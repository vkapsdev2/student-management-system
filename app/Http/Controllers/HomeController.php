<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\RoleUser;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $page_title = 'Dashboard';
        $page_description = '';

        $student       = RoleUser::where('role_id',3)->get()->count();
        $staff    = RoleUser::where('role_id',2)->get()->count();
        return view('pages.dashboard',compact('student','staff','page_title', 'page_description'));
    }
}
