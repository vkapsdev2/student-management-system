<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','japaneseName','email','age','dob','password','address','faxNumber','homeNumber','mobileNumber','gender',
        'guardianName','guardianAddress','guardianRelationship','guardianPhoneNumber','guardianFaxNumber',
        'guardianGender','guardianOccupation','companyName','companyAddress','companyPhoneNumber','emergencyName','emergencyAddress','emergencyEmail','emergencyCellphoneNumber','emergencyHomeNumber','emergencyFaxNumber','emergencyGender','emergencyRelationshipStudent'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasRole(String $roleName)
    {
        return $this->roles()->where('roles', $roleName)->exists();
    }

    public function roles()
    {
        // dd($this->belongsToMany('App\Models\Role')->get());
        return $this->belongsToMany('App\Models\Role');
    }
    public function addRole(String $roleName)
    {
        $role = Role::where('roles', $roleName)->first();

        if ($role) $this->roles()->save($role);
    }
}
