<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentAcadmicDetail extends Model
{
    use HasFactory;
    protected $table='student_acadmic_details';
    protected $fillable = [
   	'studentId','schoolName','standard','class','semesterName','semesterGrade','semesterFile','otherInformationName','otherInformationFile','schoolAchievementName','schoolAchievementFile'
    ];
}
