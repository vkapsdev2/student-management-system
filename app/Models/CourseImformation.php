<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseImformation extends Model
{
    use HasFactory;
     protected $fillable = [
     	'student_id',
        'courseName',
        'staffIncharge',
        'supportStaff',
        'typeOfClasses',
        'numOfClasses',
        'numOfHours',
        'totalnumOfHours',
        'weeklyDayClasses',
        'scheduleCalender',
        'weeklyDayTime',
        'weeklyDaysTime',
        'otherAnswer'
    ];

}
